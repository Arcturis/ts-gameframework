{
    "type": "object",
    "definitions": {
        "serverCfg": {
            "type": "object",
            "properties": {
                "port": {
                    "type": "number",
                    "description": "侦听端口号"
                }
            },
            "required": [
                "port"
            ]
        },
        "serverNode": {
            "type": "object",
            "properties": {
                "clusterNodeId": {
                    "type": "string",
                    "description": "集群节点ID（也可以视为服务器ID），集群内唯一，和该服务器启动实例配置文件中一致"
                },
                "clusterKey": {
                    "type": "string",
                    "description": "加入集群的密钥，必须和该服务器启动实例配置文件中一致"
                }
            },
            "required": [
                "clusterNodeId",
                "clusterKey"
            ]
        },
        "serverNodeList": {
            "type": "array",
            "items": {
                "$ref": "#/definitions/serverNode"
            }
        }
    },
    "required": [
        "redisConfig",
        "connString",
        "runServer"
    ],
    "properties": {
        "redisConfig": {
            "type": "object",
            "description": "连接redis服务器的配置，如果任何一个服务有需要，则需要配置",
            "properties": {
                "ssl": {
                    "type": "boolean",
                    "description": "是否启用SSL"
                },
                "host": {
                    "type": "string",
                    "description": "服务器地址（IP或者域名）"
                },
                "port": {
                    "type": "number",
                    "description": "服务器端口"
                },
                "username": {
                    "type": "string",
                    "description": "如果有开启认证，则需要配置用户名和密码"
                },
                "password": {
                    "type": "string",
                    "description": "如果有开启认证，则需要配置用户名和密码"
                },
                "database": {
                    "type": "number",
                    "description": "使用的redis的数据库，从0开始"
                }
            }
        },
        "connString": {
            "type": "object",
            "description": "配置数据库的连接字符串，如果任何一个服务有需要，则需要配置",
            "properties": {
                "appDb": {
                    "type": "object",
                    "description": "开放平台应用库，目前直接使用mysql库",
                    "properties": {
                        "mysql": {
                            "type": "string",
                            "description": "连接mysql的连接字符串，可参考地址：https://www.npmjs.com/package/mysql#connection-options"
                        }
                    }
                }
            }
        },
        "runServer": {
            "type": "array",
            "description": "当前实例要启动的服务，如: HallServer, GameServerCluster, MatchServerCluster",
            "additionalItems": false,
            "items": {
                "type": "string",
                "oneOf": [{
                        "type": "string",
                        "const": "HallServer",
                        "description": "大厅服务器, 必须配置hallServer节点"
                    },
                    {
                        "type": "string",
                        "const": "GameServerCluster",
                        "description": "游戏服务集群, 必须配置 gameServerCluster 节点"
                    },
                    {
                        "type": "string",
                        "const": "MatchServerCluster",
                        "description": "匹配服务集群, 必须配置 matchServerCluster 节点"
                    },
                    {
                        "type": "string",
                        "const": "GameServer",
                        "description": "游戏服务, 必须配置 gameServer 节点"
                    },
                    {
                        "type": "string",
                        "const": "MatchServer",
                        "description": "匹配服务, 必须配置 matchServer 节点"
                    },
                    {
                        "type": "string",
                        "const": "DemoServer",
                        "description": "示例应用的用户系统接入服务, 必须配置 demoServer 节点"
                    }
                ]
            },
            "uniqueItems": true
        },
        "hallServer": {
            "$ref": "#/definitions/serverCfg",
            "description": "大厅服务配置，如果runServer中有配置 HallServer 则需要配置本节点"
        },
        "gameServerCluster": {
            "$ref": "#/definitions/serverCfg",
            "description": "游戏服务集群配置，如果runServer中有配置 GameServerCluster 则需要配置本节点",
            "properties": {
                "nodeList": {
                    "$ref": "#/definitions/serverNodeList",
                    "description": "管理的集群节点列表，需要配置在这里才能成功登录集群"
                }
            }
        },
        "matchServerCluster": {
            "$ref": "#/definitions/serverCfg",
            "description": "匹配服务集群配置，如果runServer中有配置 MatchServerCluster 则需要配置本节点",
            "properties": {
                "nodeList": {
                    "$ref": "#/definitions/serverNodeList",
                    "description": "管理的集群节点列表，需要配置在这里才能成功登录集群"
                }
            }
        }
    },
    "allOf": [{
            "if": {
                "properties": {
                    "runServer": {
                        "contains": {
                            "const": "HallServer"
                        }
                    }
                }
            },
            "then": {
                "required": [
                    "hallServer"
                ]
            }
        },
        {
            "if": {
                "properties": {
                    "runServer": {
                        "contains": {
                            "const": "GameServerCluster"
                        }
                    }
                }
            },
            "then": {
                "required": [
                    "gameServerCluster"
                ]
            }
        },
        {
            "if": {
                "properties": {
                    "runServer": {
                        "contains": {
                            "const": "MatchServerCluster"
                        }
                    }
                }
            },
            "then": {
                "required": [
                    "matchServerCluster"
                ]
            }
        },
        {
            "if": {
                "properties": {
                    "runServer": {
                        "contains": {
                            "const": "DemoServer"
                        }
                    }
                }
            },
            "then": {
                "required": [
                    "demoServer"
                ]
            }
        }
    ]
}