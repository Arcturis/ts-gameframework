import { assert } from "chai";
import { delay, arrGroup } from "../../src/shared/tsgf/Utils";


it('delay', async () => {
    console.log(new Date().toLocaleString());
    await delay(5000);
    console.log(new Date().toLocaleString());
}, 60000);


it('groupByArr', async () => {
    let r = arrGroup([
        { key: '1', name: 'a' },
        { key: '1', name: 'b' },
        { key: '2', name: 'c' },
    ], i => i.key);
    assert.ok(r.size === 2, '应为2实为' + r.size);
    assert.ok(r.get('1')?.length === 2, '应为2实为' + r.get('1')?.length);
    assert.ok(r.get('2')?.length === 1, '应为1实为' + r.get('2')?.length);
}, 60000);
