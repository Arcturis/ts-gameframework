
import { delay } from "../../src/shared/tsgf/Utils";
import { assert } from 'chai';
import { ERoomMsgRecvType } from '../../src/shared/tsgf/room/IRoomMsg';
import { authPlayerToken, createAndEnterRoom, joinRoom } from "./ApiUtils";
import { ENetworkState } from "../../src/shared/tsgf/player/IPlayerInfo";

test('断线重连同时测试网络事件', async function () {


    //==========这里模拟服务端获取playerToken (openid需要按单元测试名区分一下,防止多个单元测试并行时token互踢)
    const auth1 = await authPlayerToken("zum0001_ApiReconnect", "zum1");
    const auth2 = await authPlayerToken("zum0002_ApiReconnect", "zum2");
    const auth3 = await authPlayerToken("zum0003_ApiReconnect", "zum3");
    //==========end
    let playerToken = auth1.playerToken;
    let playerId = auth1.playerId;
    let playerToken2 = auth2.playerToken;
    let playerToken3 = auth3.playerToken;

    let msgCount = 0;

    //玩家1创建房间并进入游戏服务器
    let gameClient1Ret = await createAndEnterRoom(playerToken, playerId, 'zum1');
    let gameClient1 = gameClient1Ret.gameClient;
    let roomId = gameClient1Ret.roomId;
    let gameClient2 = await joinRoom(playerToken2, roomId, 'zum2');
    let gameClient3 = await joinRoom(playerToken3, roomId, 'zum3');


    msgCount = 0;
    gameClient1.onChangePlayerNetworkState = (p) => {
        msgCount++;
        assert.fail('不应该收到自己的网络事件');
    };
    gameClient2.onChangePlayerNetworkState = (p) => {
        msgCount++;
        assert.ok(p.playerId === gameClient1.playerId, '这个时候应该收到玩家1的变更通知');
        assert.ok(p.networkState === ENetworkState.OFFLINE, '这个时候应该收到玩家1的下线通知');
    };
    gameClient3.onChangePlayerNetworkState = (p) => {
        msgCount++;
        assert.ok(p.playerId === gameClient1.playerId, '这个时候应该收到玩家1的变更通知');
        assert.ok(p.networkState === ENetworkState.OFFLINE, '这个时候应该收到玩家1的下线通知');
    };

    await gameClient1.client.disconnect();

    await delay(200);
    assert.ok(msgCount === 2, '剩下2个人都应该收到网络变更通知, 实际只为' + msgCount);

    msgCount = 0;
    gameClient1.onChangePlayerNetworkState = (p) => {
        msgCount++;
        assert.fail('不应该收到自己的网络事件');
    };
    gameClient2.onChangePlayerNetworkState = (p) => {
        msgCount++;
        assert.ok(p.playerId === gameClient1.playerId, '这个时候应该收到玩家1的变更通知');
        assert.ok(p.networkState === ENetworkState.ONLINE, '这个时候应该收到玩家1的上线通知');
    };
    gameClient3.onChangePlayerNetworkState = (p) => {
        msgCount++;
        assert.ok(p.playerId === gameClient1.playerId, '这个时候应该收到玩家1的变更通知');
        assert.ok(p.networkState === ENetworkState.ONLINE, '这个时候应该收到玩家1的上线通知');
    };

    let recRet = await gameClient1.reconnect();
    assert.ok(recRet.succ, '重连失败!:' + recRet.err);

    await delay(200);
    assert.ok(msgCount === 2, '剩下2个人都应该收到网络变更通知, 实际只为' + msgCount);

    //await delay(5000);//临时延迟5秒，方便我查看一下redis


    await gameClient1.disconnect();
    await gameClient2.disconnect();
    await gameClient3.disconnect();
}, 60 * 1000)