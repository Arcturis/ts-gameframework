import { assert } from "chai";
import { EMatchFromType } from "../../src/shared/tsgf/match/Models";
import { delay } from "../../src/shared/tsgf/Utils";
import { authPlayerToken, authToGameServer, authToGameServerByRoomId, createAndEnterRoom, hallClient, joinRoom } from "./ApiUtils";

it('简单创建房间', async function () {

    //==========这里模拟服务端获取playerToken (openid需要按单元测试名区分一下,防止多个单元测试并行时token互踢)
    let auth1 = await authPlayerToken("zum0001_ApiCreateRoom", "zum1");
    //==========end
    let playerToken = auth1.playerToken;
    let playerId = auth1.playerId;

    let gameClient1Ret = await createAndEnterRoom(playerToken, playerId, 'zum1', 4, false);

    await gameClient1Ret.gameClient.disconnect();

});


test('基本创建房间和加入流程', async function () {
    //==========这里模拟服务端获取playerToken (openid需要按单元测试名区分一下,防止多个单元测试并行时token互踢)
    let auth1 = await authPlayerToken("zum0001_ApiCreateRoomAndJoin", "zum1");
    let auth2 = await authPlayerToken("zum0002_ApiCreateRoomAndJoin", "zum2");
    //==========end
    let playerToken1 = auth1.playerToken;
    let playerId1 = auth1.playerId;
    let playerToken2 = auth2.playerToken;
    let playerId2 = auth2.playerId;

    //创建并进入房间
    let gameClient1Ret = await createAndEnterRoom(playerToken1, playerId1, 'zum1', 2, false);
    let gameClient1 = gameClient1Ret.gameClient;

    //玩家2加入玩家1创建好的房间
    let gameClient2 = await joinRoom(playerToken2, gameClient1Ret.roomId, 'zum2');

    await gameClient1.disconnect();
    await gameClient2.disconnect();
});


test('房间加入失败_MaxPlayers1', async function () {
    //==========这里模拟服务端获取playerToken (openid需要按单元测试名区分一下,防止多个单元测试并行时token互踢)
    let auth1 = await authPlayerToken("zum0001_ApiCreateRoomAndJoin_MaxPlayers", "zum1");
    let auth2 = await authPlayerToken("zum0002_ApiCreateRoomAndJoin_MaxPlayers", "zum2");
    //==========end
    let playerToken1 = auth1.playerToken;
    let playerId1 = auth1.playerId;
    let playerToken2 = auth2.playerToken;
    let playerId2 = auth2.playerId;

    //创建单人房间
    let gameClient1Ret = await createAndEnterRoom(playerToken1, playerId1, 'zum1', 1, false);
    let gameClient1 = gameClient1Ret.gameClient;

    //玩家2
    let gameClient2 = await authToGameServerByRoomId(playerToken2, gameClient1Ret.roomId, 'zum2');
    let ret2 = await gameClient2.joinRoom(gameClient1Ret.roomId);
    assert.ok(ret2.succ === false && ret2.code === 1001, '加入应该失败的!因为最大人数只有1!');

    await gameClient1.disconnect();
    await gameClient2.disconnect();
});

test('房间加入失败_MaxPlayers2', async function () {
    //==========这里模拟服务端获取playerToken (openid需要按单元测试名区分一下,防止多个单元测试并行时token互踢)
    let auth1 = await authPlayerToken("zum0001_ApiCreateRoomAndJoin_MaxPlayers2", "zum1");
    let auth2 = await authPlayerToken("zum0002_ApiCreateRoomAndJoin_MaxPlayers2", "zum2");
    let auth3 = await authPlayerToken("zum0003_ApiCreateRoomAndJoin_MaxPlayers2", "zum3");
    let auth4 = await authPlayerToken("zum0004_ApiCreateRoomAndJoin_MaxPlayers2", "zum4");
    //==========end
    let playerToken1 = auth1.playerToken;
    let playerId1 = auth1.playerId;
    let playerToken2 = auth2.playerToken;
    let playerId2 = auth2.playerId;
    let playerToken3 = auth3.playerToken;
    let playerId3 = auth3.playerId;
    let playerToken4 = auth4.playerToken;


    //创建单人房间
    let gameClient1Ret = await createAndEnterRoom(playerToken1, playerId1, 'zum1', 2, false);
    let gameClient1 = gameClient1Ret.gameClient;
    let roomId = gameClient1Ret.roomId;
    //玩家2正常加入
    let gameClient2 = await joinRoom(playerToken2, roomId, 'zum2');

    //玩家4先加入到房间所在游戏服务器(先备用)
    let gameClient4 = await authToGameServer('zum4', playerToken4, gameClient1Ret.gameServerUrl);

    //玩家3加入失败
    let gameClient3 = await authToGameServerByRoomId(playerToken3, roomId, 'zum3');
    let ret3 = await gameClient3.joinRoom(roomId);
    assert.ok(ret3.succ === false && ret3.code === 1001, '加入应该失败的!因为最大人数只有2!');

    //玩家2主动退出房间,让玩家3再试,应该要能加入
    await gameClient2.leaveRoom();
    ret3 = await gameClient3.joinRoom(roomId);
    assert.ok(ret3.succ, ret3.err);

    //这个时候玩家2再去加入,应该要失败
    let ret2 = await gameClient2.joinRoom(roomId);
    assert.ok(ret2.succ === false && ret2.code === 1001, '加入应该失败的!因为最大人数只有2!');

    //玩家3直接断开,玩家2应该要能加入成功
    await gameClient3.disconnect();
    ret2 = await gameClient2.joinRoom(roomId);
    assert.ok(ret2.succ, ret2.err);

    //玩家1和玩家2都断开,这个时候房间应该被解散了
    await gameClient1.disconnect();
    await gameClient2.disconnect();

    //重新连应该是失败的,先用大厅获取房间注册信息,应该也是要销毁的
    let regRet = await hallClient.getRoomRegInfo(playerToken1, roomId);
    assert.ok(regRet.succ === false, '大厅获取房间注册信息应该是要返回不存在!');
    assert.ok(regRet.code === 2002, '大厅获取房间注册信息应该是要返回不存在!但返回的错误码是' + regRet.code);
    //然后用玩家4(前面已经连接到同一台游戏服务器的),尝试加入房间,应该也是要失败的
    let ret4 = await gameClient4.joinRoom(roomId);
    assert.ok(ret4.succ === false && ret4.code === 2002, '游戏服务器的加入房间应该是要返回不存在!');

});
