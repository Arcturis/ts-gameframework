
import { delay } from "../../src/shared/tsgf/Utils";
import { assert } from 'chai';
import { authPlayerToken, cancelMatch, hallClient, joinRoom, joinRoomUseGameServer, queryMatch, requestMatchOneBaseMelee } from "./ApiUtils";
import { ERoomMsgRecvType } from "../../src/shared/tsgf/room/IRoomMsg";


test('多个单元测试,因为防止并行,所以都放在一个文件', async function () {
    //注意:同个匹配器的,需要串行执行,防止并行带来的结果非预期

    //模拟三个客户端单人混战匹配
    {
        //==========这里模拟服务端获取playerToken (openid需要按单元测试名区分一下,防止多个单元测试并行时token互踢)
        const auth1 = await authPlayerToken("zum0001_ApiPlayerMatch", "zum1");
        const auth2 = await authPlayerToken("zum0002_ApiPlayerMatch", "zum2");
        const auth3 = await authPlayerToken("zum0003_ApiPlayerMatch", "zum3");
        //==========end
        let playerToken1 = auth1.playerToken;
        let playerId1 = auth1.playerId;
        let playerToken2 = auth2.playerToken;
        let playerId2 = auth2.playerId;
        let playerToken3 = auth3.playerToken;
        let playerId3 = auth3.playerId;

        //匹配请求
        let matchReqId1 = await requestMatchOneBaseMelee(playerToken1, playerId1, 8, 3);
        let matchReqId2 = await requestMatchOneBaseMelee(playerToken2, playerId2, 8, 3);
        let matchReqId3 = await requestMatchOneBaseMelee(playerToken3, playerId3, 8, 3);

        //延时1秒
        await delay(1000);

        let matchRet1 = await queryMatch(playerToken1, matchReqId1);
        let matchRet2 = await queryMatch(playerToken2, matchReqId2);
        let matchRet3 = await queryMatch(playerToken3, matchReqId3);

        console.log('matchRet1', matchRet1);
        console.log('matchRet2', matchRet2);
        console.log('matchRet3', matchRet3);

        let gameClient1 = await joinRoomUseGameServer(matchRet1.gameServerUrl, playerToken1, matchRet1.roomId, 'zum1');
        let gameClient2 = await joinRoomUseGameServer(matchRet2.gameServerUrl, playerToken2, matchRet2.roomId, 'zum2');
        let gameClient3 = await joinRoomUseGameServer(matchRet3.gameServerUrl, playerToken3, matchRet3.roomId, 'zum3');

        await gameClient3.sendRoomMsg({
            recvType: ERoomMsgRecvType.ROOM_OTHERS,
            msg: '大伙好呀~'
        });

        await gameClient1.disconnect();
        await gameClient2.disconnect();
        await gameClient3.disconnect();

    }

    //取消匹配
    {
        //==========这里模拟服务端获取playerToken (openid需要按单元测试名区分一下,防止多个单元测试并行时token互踢)
        const auth1 = await authPlayerToken("zum0001_ApiPlayerMatchRoomJoinUs", "zum1");
        const auth2 = await authPlayerToken("zum0002_ApiPlayerMatchRoomJoinUs", "zum2");
        const auth3 = await authPlayerToken("zum0003_ApiPlayerMatchRoomJoinUs", "zum3");
        //==========end
        let playerToken1 = auth1.playerToken;
        let playerId1 = auth1.playerId;
        let playerToken2 = auth2.playerToken;
        let playerId2 = auth2.playerId;
        let playerToken3 = auth3.playerToken;
        let playerId3 = auth3.playerId;

        let matchReqId1 = await requestMatchOneBaseMelee(playerToken1, playerId1, 8, 3);
        let matchReqId2 = await requestMatchOneBaseMelee(playerToken2, playerId2, 8, 3);
        //取消玩家1匹配
        await cancelMatch(playerToken1, matchReqId1);
        let matchReqId3 = await requestMatchOneBaseMelee(playerToken3, playerId3, 8, 3);

        //延时
        await delay(500);

        //这个时候不满3个,有2个在匹配池等待中,所以没有结果
        let retM1 = await hallClient.queryMatch(playerToken1, matchReqId1);
        let retM2 = await hallClient.queryMatch(playerToken2, matchReqId2);
        let retM3 = await hallClient.queryMatch(playerToken3, matchReqId3);
        assert.ok(retM1 === null, '应该查不到结果的!' + JSON.stringify(retM1));
        assert.ok(retM2 === null, '应该查不到结果的!' + JSON.stringify(retM2));
        assert.ok(retM3 === null, '应该查不到结果的!' + JSON.stringify(retM3));

        //玩家1再提交匹配
        matchReqId1 = await requestMatchOneBaseMelee(playerToken1, playerId1, 8, 3);

        //延时
        await delay(500);

        //这个时候应该有结果了
        retM1 = await hallClient.queryMatch(playerToken1, matchReqId1);
        retM2 = await hallClient.queryMatch(playerToken2, matchReqId2);
        retM3 = await hallClient.queryMatch(playerToken3, matchReqId3);
        assert.ok(retM1 !== null, '应该要有结果了!');
        assert.ok(retM1?.succ === true, '应该匹配成功才对!' + retM1?.err);
        assert.ok(retM2 !== null, '应该要有结果了!');
        assert.ok(retM2?.succ === true, '应该匹配成功才对!' + retM2?.err);
        assert.ok(retM3 !== null, '应该要有结果了!');
        assert.ok(retM3?.succ === true, '应该匹配成功才对!' + retM3?.err);

    }


    //4max,min2,2人匹配,应成功,再进入招人匹配,依次进入2人,都要成功,再进1人应该要没结果,再取消!
    {
        //==========这里模拟服务端获取playerToken (openid需要按单元测试名区分一下,防止多个单元测试并行时token互踢)
        const auth1 = await authPlayerToken("zum0001_ApiPlayerMatch", "zum1");
        const auth2 = await authPlayerToken("zum0002_ApiPlayerMatch", "zum2");
        const auth3 = await authPlayerToken("zum0003_ApiPlayerMatch", "zum3");
        const auth4 = await authPlayerToken("zum0004_ApiPlayerMatch", "zum4");
        const auth5 = await authPlayerToken("zum0005_ApiPlayerMatch", "zum5");
        //==========end
        let playerToken1 = auth1.playerToken;
        let playerId1 = auth1.playerId;
        let playerToken2 = auth2.playerToken;
        let playerId2 = auth2.playerId;
        let playerToken3 = auth3.playerToken;
        let playerId3 = auth3.playerId;
        let playerToken4 = auth4.playerToken;
        let playerId4 = auth4.playerId;
        let playerToken5 = auth5.playerToken;
        let playerId5 = auth5.playerId;

        //匹配请求
        let matchReqId1 = await requestMatchOneBaseMelee(playerToken1, playerId1, 4, 2);
        let matchReqId2 = await requestMatchOneBaseMelee(playerToken2, playerId2, 4, 2);
        await delay(1000);
        let matchRet1 = await queryMatch(playerToken1, matchReqId1);
        let matchRet2 = await queryMatch(playerToken2, matchReqId2);

        let gameClient1 = await joinRoomUseGameServer(matchRet1.gameServerUrl, playerToken1, matchRet1.roomId, 'zum1');
        let gameClient2 = await joinRoomUseGameServer(matchRet2.gameServerUrl, playerToken2, matchRet2.roomId, 'zum2');

        let matchReqId3 = await requestMatchOneBaseMelee(playerToken3, playerId3, 4, 2);
        await delay(1000);
        let matchRet3 = await queryMatch(playerToken3, matchReqId3);
        let gameClient3 = await joinRoomUseGameServer(matchRet3.gameServerUrl, playerToken3, matchRet3.roomId, 'zum3');

        let matchReqId4 = await requestMatchOneBaseMelee(playerToken4, playerId4, 4, 2);
        await delay(1000);
        let matchRet4 = await queryMatch(playerToken4, matchReqId4);
        let gameClient4 = await joinRoomUseGameServer(matchRet4.gameServerUrl, playerToken4, matchRet4.roomId, 'zum4');

        let matchReqId5 = await requestMatchOneBaseMelee(playerToken5, playerId5, 4, 2);
        await delay(1000);
        let retM5 = await hallClient.queryMatch(playerToken5, matchReqId5);
        assert.ok(retM5 === null, "应该没结果!实际有了!");
        //取消匹配
        await cancelMatch(playerToken5, matchReqId5);

        //这个时候退出了一个
        await gameClient4.leaveRoom();

        //玩家5这个时候要能匹配进房间才对!
        matchReqId5 = await requestMatchOneBaseMelee(playerToken5, playerId5, 4, 2);
        await delay(1000);
        let matchRet5 = await queryMatch(playerToken5, matchReqId5);
        let gameClient5 = await joinRoomUseGameServer(matchRet5.gameServerUrl, playerToken5, matchRet5.roomId, 'zum5');


        await gameClient1.disconnect();
        await gameClient2.disconnect();
        await gameClient3.disconnect();
        await gameClient4.disconnect();
        await gameClient5.disconnect();

    }
}, 60 * 1000);
