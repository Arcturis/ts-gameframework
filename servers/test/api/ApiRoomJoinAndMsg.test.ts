
import { delay } from "../../src/shared/tsgf/Utils";
import { assert } from 'chai';
import { ERoomMsgRecvType } from '../../src/shared/tsgf/room/IRoomMsg';
import { authPlayerToken, createAndEnterRoom, joinRoom } from "./ApiUtils";

test('创建房间和多玩家加入房间以及事件触发逻辑', async function () {


    //==========这里模拟服务端获取playerToken (openid需要按单元测试名区分一下,防止多个单元测试并行时token互踢)
    const auth1 = await authPlayerToken("zum0001_ApiRoomJoinAndMsg", "zum1");
    const auth2 = await authPlayerToken("zum0002_ApiRoomJoinAndMsg", "zum2");
    const auth3 = await authPlayerToken("zum0003_ApiRoomJoinAndMsg", "zum3");
    //==========end
    let playerToken = auth1.playerToken;
    let playerId = auth1.playerId;
    let playerToken2 = auth2.playerToken;
    let playerToken3 = auth3.playerToken;

    //玩家1创建房间并进入游戏服务器
    let gameClient1Ret = await createAndEnterRoom(playerToken, playerId, 'zum1');
    let gameClient1 = gameClient1Ret.gameClient;
    let roomId = gameClient1Ret.roomId;

    //玩家2加入玩家1创建的房间（进入游戏服务器），玩家1收到来人通知
    let msgCount = 0;
    gameClient1.onPlayerJoinRoom = (playerInfo, roomInfo) => {
        assert.ok(playerInfo.playerId === auth2.playerId, '这个时候【玩家1】应该收到【玩家2】的进入房间通知');
        msgCount++;
    };
    let gameClient2 = await joinRoom(playerToken2, roomId, 'zum2');
    await delay(200);//延时一下，因为通知消息是异步的，不会等待本rpc回来再通知
    assert.ok(msgCount === 1, '应该要收到1个消息！');

    msgCount = 0;
    gameClient1.onPlayerJoinRoom = (playerInfo, roomInfo) => {
        assert.ok(playerInfo.playerId === auth3.playerId, '这个时候【玩家1】应该收到【玩家3】的进入房间通知');
        msgCount++;
    };
    gameClient2.onPlayerJoinRoom = (playerInfo, roomInfo) => {
        assert.ok(playerInfo.playerId === auth3.playerId, '这个时候【玩家2】应该收到【玩家3】的进入房间通知');
        msgCount++;
    };
    //玩家3加入玩家1创建的房间（进入游戏服务器），玩家2、玩家2收到来人通知
    let gameClient3 = await joinRoom(playerToken3, roomId, 'zum3');
    await delay(200);//延时一下，因为通知消息是异步的，不会等待本rpc回来再通知
    assert.ok(msgCount === 2, '应该要收到2个消息！');

    //玩家2发送房间消息
    msgCount = 0;
    gameClient1.onRecvRoomMsg = (msg) => {
        assert.ok(msg.fromPlayerInfo.playerId === auth2.playerId, '这个时候【玩家1】应该收到【玩家2】的房间广播消息');
        msgCount++;
    };
    gameClient2.onRecvRoomMsg = (msg) => {
        assert.ok(msg.fromPlayerInfo.playerId === auth2.playerId, '这个时候【玩家2】应该收到【玩家2】的房间广播消息');
        msgCount++;
    };
    gameClient3.onRecvRoomMsg = (msg) => {
        assert.ok(msg.fromPlayerInfo.playerId === auth2.playerId, '这个时候【玩家3】应该收到【玩家2】的房间广播消息');
        msgCount++;
    };
    await gameClient2.sendRoomMsg({
        recvType: ERoomMsgRecvType.ROOM_ALL,
        msg: '广播所有人一条测试消息'
    });
    await delay(200);//延时一下，因为通知消息是异步的，不会等待本rpc回来再通知
    assert.ok(msgCount === 3, '应该要收到3个消息！');


    msgCount = 0;
    gameClient1.onRecvRoomMsg = (msg) => {
        assert.ok(msg.fromPlayerInfo.playerId === auth2.playerId, '这个时候【玩家1】应该收到【玩家2】的房间给他人的消息');
        msgCount++;
    };
    gameClient2.onRecvRoomMsg = (msg) => {
        assert.fail('这个时候【玩家2】不应该收到【玩家2】的房间消息');
    };
    gameClient3.onRecvRoomMsg = (msg) => {
        assert.ok(msg.fromPlayerInfo.playerId === auth2.playerId, '这个时候【玩家3】应该收到【玩家2】的房间给他人的消息');
        msgCount++;
    };
    await gameClient2.sendRoomMsg({
        recvType: ERoomMsgRecvType.ROOM_OTHERS,
        msg: '其他人，我是玩家1'
    });
    await delay(200);//延时一下，因为通知消息是异步的，不会等待本rpc回来再通知
    assert.ok(msgCount === 2, '应该要收到2个消息！');


    msgCount = 0;
    gameClient1.onRecvRoomMsg = (msg) => {
        assert.ok(msg.fromPlayerInfo.playerId === auth2.playerId, '这个时候【玩家1】应该收到【玩家2】的房间指定消息');
        msgCount++;
    };
    gameClient2.onRecvRoomMsg = (msg) => {
        assert.fail('这个时候【玩家2】不应该收到【玩家2】的房间消息');
    };
    gameClient3.onRecvRoomMsg = (msg) => {
        assert.fail('这个时候【玩家3】不应该收到【玩家2】的房间消息');
    };
    await gameClient2.sendRoomMsg({
        recvType: ERoomMsgRecvType.ROOM_SOME,
        recvPlayerList: [gameClient2.currRoomInfo!.ownerPlayerId],
        msg: '房主好，我是玩家1',
    });
    await delay(200);//延时一下，因为通知消息是异步的，不会等待本rpc回来再通知
    assert.ok(msgCount === 1, '应该要收到1个消息！');


    //玩家2离开房间, 玩家1、玩家2收到来人通知
    msgCount = 0;
    gameClient1.onPlayerLeaveRoom = (playerInfo, roomInfo) => {
        assert.ok(playerInfo.playerId === auth2.playerId, '这个时候【玩家1】应该收到【玩家2】的退出房间通知');
        msgCount++;
    };
    gameClient2.onPlayerLeaveRoom = (playerInfo, roomInfo) => {
        assert.fail('这个时候【玩家2】不应该收到【玩家2】的退出房间通知');
    };
    gameClient3.onPlayerLeaveRoom = (playerInfo, roomInfo) => {
        assert.ok(playerInfo.playerId === auth2.playerId, '这个时候【玩家3】应该收到【玩家2】的退出房间通知');
        msgCount++;
    };
    await gameClient2.leaveRoom();
    await delay(200);//延时一下，因为通知消息是异步的，不会等待本rpc回来再通知
    assert.ok(msgCount === 2, `应该要收到2个消息！实际${msgCount}个`);

    //玩家1解散房间，玩家3收到解散通知
    msgCount = 0;
    gameClient1.onDismissRoom = (roomInfo) => {
        assert.fail('这个时候【玩家1】不应该收到解散房间通知');
    };
    gameClient2.onDismissRoom = (roomInfo) => {
        assert.fail('这个时候【玩家2】不应该收到解散房间通知');
    };
    gameClient3.onDismissRoom = (roomInfo) => {
        msgCount++;
    };
    await gameClient1.dismissRoom();
    await delay(200);//延时一下，因为通知消息是异步的，不会等待本rpc回来再通知
    assert.ok(msgCount === 1, '应该要收到1个消息！');

    //await delay(5000);//临时延迟5秒，方便我查看一下redis


    await gameClient1.disconnect();
    await gameClient2.disconnect();
    await gameClient3.disconnect();
}, 60 * 1000)