import { WsServerOptions } from "tsrpc";
import { IGameServerInfo } from "../shared/hallClient/Models";
import { ClusterMgr, IClusterNodeCfg, IClusterNodeInfo } from "../shared/tsgfServer/cluster/ClusterMgr";
import { ServiceType as ClusterServiceType } from "../shared/tsgfServer/cluster/protocols/serviceProto";
import { RedisClient } from "../shared/tsgfServer/redisHelper";


/**游戏服务器管理节点，依赖redis功能*/
export class GameServerClusterMgr extends ClusterMgr<IGameServerInfo>{

    constructor(
        getNodesCfg: () => IClusterNodeCfg[],
        serverOption: Partial<WsServerOptions<ClusterServiceType>>,
        getRedisClient: () => Promise<RedisClient>) {
        super("GameServer", getNodesCfg, serverOption, getRedisClient);
    }

    /**
     * 从redis中获取所有游戏服务器列表。分布式时，大厅服务器和游戏服务器管理节点，可能不在一个服务实例上，所以使用本方法来跨服获取
     * @date 2022/4/20 - 16:48:25
     *
     * @public
     * @static
     * @async
     * @template NodeInfo
     * @param {string} clusterTypeKey 集群类型标识，用在各种场合进行区分的。需要和构造ClussterMgr时的值一致
     * @returns {Promise<IClusterNodeInfo<IGameServerInfo>[]>}
     */
    public static async getServersFromRedis(getRedisClient: () => Promise<RedisClient>): Promise<IClusterNodeInfo<IGameServerInfo>[]> {
        let list = await ClusterMgr.getNodeInfosFromRedis<IGameServerInfo>("GameServer", getRedisClient);
        return list;
    }
    /**
     * 从redis中获取指定游戏服务器信息。分布式时，大厅服务器和游戏服务器管理节点，可能不在一个服务实例上，所以使用本方法来跨服获取
     * @date 2022/4/20 - 16:48:25
     *
     * @public
     * @static
     * @async
     * @template NodeInfo
     * @param {string} clusterTypeKey 集群类型标识，用在各种场合进行区分的。需要和构造ClussterMgr时的值一致
     * @returns {Promise<IClusterNodeInfo<IGameServerInfo>[]>}
     */
    public static async getServerInfoFromRedis(serverNodeId: string, getRedisClient: () => Promise<RedisClient>)
        : Promise<IClusterNodeInfo<IGameServerInfo> | null> {
        let node = await ClusterMgr.getNodeInfoFromRedis<IGameServerInfo>("GameServer", serverNodeId, getRedisClient);
        return node;
    }
}