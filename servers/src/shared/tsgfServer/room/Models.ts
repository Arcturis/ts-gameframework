

/**房间游戏服务器注册信息，由大厅注册分配给游戏服务器使用，用于大厅能查询到房间ID关联的游戏服务器*/
export interface IRoomGameServerRegInfo {
    /**房间ID，全局唯一*/
    roomId: string;
    /**所属应用id*/
    appId: string;
    /**所属玩家ID*/
    ownerPlayerId: string;
    /**挂在哪个游戏服务器下（服务器节点id）*/
    gameServerNodeId: string;
    /**创建时间*/
    createTime: number;
    /**当前房间的玩家ID列表*/
    currPlayerIds:string[];
}