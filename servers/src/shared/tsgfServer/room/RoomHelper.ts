import { ICancelable } from "../../tsgf/ICancelable";
import { IResult } from "../../tsgf/Result";
import { EFrameSyncState, ERoomCreateType, ICreateRoomPara, IRoomInfo, IRoomRegInfo } from "../../tsgf/room/IRoomInfo";
import { RedisClient } from "../redisHelper";
import { buildGuid } from "../ServerUtils";
import { IRoomGameServerRegInfo } from "./Models";


/**房间注册信息和房间信息的结合体*/
export interface IRoomInfoPack {
    regInfo: IRoomGameServerRegInfo;
    roomInfo: IRoomInfo;
}
/*
export interface IRoomRegChanged {
    regInfo: IRoomGameServerRegInfo;
    isCreateOrChange: boolean;
    isDelete: boolean;
}
*/
export enum ERoomRegChangedType {
    Create = 1,
    Delete = 2,
    PlayerJoinRoom = 3,
    PlayerLeaveRoom = 4,
}
export type IRoomRegChanged = {
    regInfo: IRoomGameServerRegInfo;
} & ({
    changedType: ERoomRegChangedType.Create;
} | {
    changedType: ERoomRegChangedType.Delete;
} | {
    changedType: ERoomRegChangedType.PlayerJoinRoom;
    joinRoomPlayerId: string;
} | {
    changedType: ERoomRegChangedType.PlayerLeaveRoom;
    leaveRoomPlayerId: string;
});

/**房间公共操作（跨服务器）*/
export class RoomHelper {

    private static getRedisClient: (reuseClient: boolean) => Promise<RedisClient>;
    private static oneDaySec = 24 * 60 * 60;
    private static oneHourSec = 60 * 60;

    public static init(getRedisClient: (reuseClient: boolean) => Promise<RedisClient>) {
        RoomHelper.getRedisClient = getRedisClient;
    }

    public static buildRegRoomRedisKey(roomId: string) {
        return `Rooms:Reg:roomId_${roomId}`;
    }
    public static buildCreateRoomInfoRedisKey(roomId: string) {
        return `Rooms:CreateInfo:roomId_${roomId}`;
    }


    /**
     * 构建创建房间的相关信息（房间注册信息和初始的房间信息）
     * @date 2022/5/12 - 15:37:07
     *
     * @public
     * @static
     * @param {string} appId
     * @param {string} gameServerNodeId
     * @param {ICreateRoomPara} para
     * @returns {{ regInfo: IRoomGameServerRegInfo, roomInfo: IRoomInfo }}
     */
    public static buildRoomInfo(appId: string, gameServerNodeId: string, para: ICreateRoomPara)
        : IRoomInfoPack {
        let roomId = buildGuid('Room_');
        return {
            regInfo: {
                roomId: roomId,
                appId: appId,
                ownerPlayerId: para.ownerPlayerId,
                gameServerNodeId: gameServerNodeId,
                createTime: Date.now(),
                currPlayerIds: [],
            },
            roomInfo: {
                roomId: roomId,
                roomName: para.roomName,

                roomType: para.roomType,
                createType: ERoomCreateType.COMMON_CREATE,
                maxPlayers: para.maxPlayers,
                customProperties: para.customProperties,
                ownerPlayerId: para.ownerPlayerId,
                isPrivate: para.isPrivate,
                matcherKey: para.matcherKey,
                isForbidJoin: false,

                playerList: [],

                frameRate: 60,
                frameSyncState: EFrameSyncState.STOP,
                createTime: Date.now(),
                startGameTime: 0,
            },
        };
    }

    /**
     * 注册服务器房间信息，注册完后，玩家才可以连接到对应游戏服务器进入房间
     * @date 2022/5/10 - 14:46:25
     *
     * @public
     * @static
     * @param {IRoomGameServerRegInfo} roomRegInfo 房间的注册信息，会在redis里保留1天，即房间1天内都可以加人等操作
     * @param {IRoomInfo} roomInfo 创建时的房间信息，被游戏服务器拉取后就删除了
     */
    public static async regRoom(roomRegInfo: IRoomGameServerRegInfo, roomInfo: IRoomInfo) {
        let regKey = RoomHelper.buildRegRoomRedisKey(roomRegInfo.roomId);
        let roomKey = RoomHelper.buildCreateRoomInfoRedisKey(roomRegInfo.roomId);
        let client = await RoomHelper.getRedisClient(true);
        await client.setObject(regKey, roomRegInfo, RoomHelper.oneDaySec);
        await client.setObject(roomKey, roomInfo, RoomHelper.oneHourSec);
        client.publishObject('RoomRegInfoChanged', {
            changedType: ERoomRegChangedType.Create,
            regInfo: roomRegInfo,
        } as IRoomRegChanged);
    }
    /**
     * 更新房间注册信息, 玩家进入房间造成的更新
     * @date 2022/5/10 - 14:46:25
     *
     * @public
     * @static
     * @param {IRoomGameServerRegInfo} roomRegInfo 房间的注册信息，会在redis里保留1天，即房间1天内都可以加人等操作
     */
    public static async updateRoomRegInfoFromJoinPlayer(roomRegInfo: IRoomGameServerRegInfo, joinRoomPlayerId: string) {
        let regKey = RoomHelper.buildRegRoomRedisKey(roomRegInfo.roomId);
        let client = await RoomHelper.getRedisClient(true);
        await client.setObject(regKey, roomRegInfo, RoomHelper.oneDaySec);
        client.publishObject('RoomRegInfoChanged', {
            changedType: ERoomRegChangedType.PlayerJoinRoom,
            regInfo: roomRegInfo,
            joinRoomPlayerId: joinRoomPlayerId,
        } as IRoomRegChanged);
    }
    /**
     * 更新房间注册信息, 玩家离开房间造成的更新
     * @date 2022/5/10 - 14:46:25
     *
     * @public
     * @static
     * @param {IRoomGameServerRegInfo} roomRegInfo 房间的注册信息，会在redis里保留1天，即房间1天内都可以加人等操作
     */
    public static async updateRoomRegInfoFromLeavePlayer(roomRegInfo: IRoomGameServerRegInfo, leaveRoomPlayerId: string) {
        let regKey = RoomHelper.buildRegRoomRedisKey(roomRegInfo.roomId);
        let client = await RoomHelper.getRedisClient(true);
        await client.setObject(regKey, roomRegInfo, RoomHelper.oneDaySec);
        client.publishObject('RoomRegInfoChanged', {
            changedType: ERoomRegChangedType.PlayerLeaveRoom,
            regInfo: roomRegInfo,
            leaveRoomPlayerId: leaveRoomPlayerId,
        } as IRoomRegChanged);
    }

    /**
     * 查询房间注册信息
     * @date 2022/5/10 - 15:14:18
     *
     * @public
     * @static
     * @async
     * @param {string} roomId
     * @returns {(Promise<IRoomGameServerRegInfo | null>)}
     */
    public static async getRoomRegInfo(roomId: string): Promise<IRoomGameServerRegInfo | null> {
        let regKey = RoomHelper.buildRegRoomRedisKey(roomId);
        let client = await RoomHelper.getRedisClient(true);
        let regInfo = await client.getObject<IRoomGameServerRegInfo>(regKey);
        return regInfo;
    }

    /**
     * [游戏服务器]提取已经注册的房间信息, 提取完后不可再次提取
     * @date 2022/5/10 - 15:14:18
     *
     * @public
     * @static
     * @async
     * @param {string} roomId
     * @param {string} gameServerNodeId 传入当前游戏服务器节点ID，验证房间是否分配给这个节点的，没通过返回null
     * @returns {(Promise<IRoomInfo | null>)}
     */
    public static async extractRoomInfo(roomId: string, gameServerNodeId: string)
        : Promise<{ roomInfo: IRoomInfo, regInfo: IRoomGameServerRegInfo } | null> {
        let regKey = RoomHelper.buildRegRoomRedisKey(roomId);
        let roomKey = RoomHelper.buildCreateRoomInfoRedisKey(roomId);
        let client = await RoomHelper.getRedisClient(true);
        let regInfo = await client.getObject<IRoomGameServerRegInfo>(regKey);
        if (!regInfo || regInfo.gameServerNodeId != gameServerNodeId) {
            //没注册信息或者不是给这个游戏服务器ID的, 返回null
            return null;
        }
        let roomInfo = await client.getObject<IRoomInfo>(roomKey);
        if (!roomInfo) {
            return null;
        }
        client.delete(roomKey);
        return {
            roomInfo,
            regInfo,
        };
    }


    /**
     * 删除房间注册信息(只删除redis里保存的信息，让大厅无法获取到信息，游戏服务器部分需要自己处理，所以本方法一般由房间所属游戏服务器调用)
     * @date 2022/5/10 - 15:14:18
     *
     * @public
     * @static
     * @async
     * @param {string} roomId
     * @returns {(Promise<IRoomInfo | null>)}
     */
    public static async deleteRoomRegInfo(roomId: string): Promise<void> {
        let client = await RoomHelper.getRedisClient(true);
        let regKey = RoomHelper.buildRegRoomRedisKey(roomId);
        let regInfo = await client.getObject<IRoomGameServerRegInfo>(regKey);
        let roomKey = RoomHelper.buildCreateRoomInfoRedisKey(roomId);
        await client.delete(regKey, roomKey);
        if (regInfo) {
            //有注册信息才推送通知!
            client.publishObject('RoomRegInfoChanged', {
                changedType: ERoomRegChangedType.Delete,
                regInfo: regInfo,
            } as IRoomRegChanged);
        }
    }


    /**
     * 开始侦听全局的房间注册信息变更事件(跨服务器), 返回取消侦听的对象,需要自行保存!
     * @date 2022/5/5 - 16:03:57
     *
     * @public
     * @async
     * @param {(notify: IMatchResultNotify) => void} listen
     * @returns {Promise<void>}
     */
    public static async startListenRoomRegInfoChanged(listen: (changedInfo: IRoomRegChanged) => void): Promise<ICancelable> {
        let subscribeClient = await this.getRedisClient(false);
        await subscribeClient.subscribeObject<IRoomRegChanged>("RoomRegInfoChanged", listen);
        return {
            async cancel(): Promise<void> {
                await subscribeClient.disconnect();
            }
        };
    }

}