
import { IMatchFromPlayer, IMatchFromRoomJoinUs, IMatchParams, IMatchParamsBase, IMatchParamsFromPlayer, EMatchFromType, IMatchResult } from "../../tsgf/match/Models";
import { IResult } from "../../tsgf/Result";
import { ICreateRoomPara } from "../../tsgf/room/IRoomInfo";


export interface IMatchFromRoomJoinUsOnServer extends IMatchFromRoomJoinUs {
    /**当前应有玩家数量(匹配管理器会自动更新), 包含匹配进来(但可能还没连到服务器)和自主进入的玩家*/
    currPlayerCount: number;
    /**当前匹配进入的玩家ID列表*/
    matchPlayerIds:string[];
}
export interface IMatchParamsFromRoomJoinUs extends IMatchParamsBase {
    /**发起类型是房间招人*/
    matchFromType: EMatchFromType.RoomJoinUs;
    /**匹配发起的玩家信息*/
    matchFromInfo: IMatchFromRoomJoinUsOnServer;
}


/**匹配请求*/
export type IMatchRequest =
    (IMatchParamsFromPlayer | IMatchParamsFromRoomJoinUs)
    & {
        /**匹配请求ID，到服务端时生成唯一ID*/
        matchReqId: string;
        /**请求匹配时间(毫秒级时间戳)*/
        requestTime: number;
        /**开始匹配时间(毫秒级时间戳)，只有匹配服务器收到请求后才有值*/
        startMatchTime: number;
    };

/**匹配操作类型*/
export enum EMatchProcType {
    /**发起匹配*/
    RequestMatch = 1,
    /**取消匹配*/
    CancelMatch = 2,
}

/**请求匹配操作*/
interface IRequestMatchProc {
    procType: EMatchProcType.RequestMatch;
    /**匹配请求ID, 匹配请求数据已经放在全局,调用 MatchRequestHelper 来获取全局匹配请求数据 (防止请求先被取消,队列才收到)*/
    matchReqId: string;
}
/**取消匹配操作*/
interface ICancelMatchProc {
    procType: EMatchProcType.CancelMatch;
    matchReqId: string;
}

/**匹配操作*/
export type IMatchProc = IRequestMatchProc | ICancelMatchProc;


/**匹配器执行结果*/
export interface IMatcherExecResult {
    /**匹配是否有结果了*/
    hasResult: boolean;
    /**匹配结果错误时放错误消息*/
    resultErrMsg?: string | null;
    /**
     * 匹配结果错误时放错误码
     * 
     * ==1000+的匹配错误码，都是可以重试的
     * 1001: 请求被取消（可能是服务器变动等原因）, 这种错误码，不用弹提示，直接重试即可
     * 1002: 游戏服务器爆满，请稍后再试！
     * 1003: 匹配超时！
     * 
     * ==2000+的匹配错误码，都不可以重试的输入参数错误类型
     * 2000: 未知
     * 2001: 匹配器标识不存在！
     * 2002: matchFromInfo.roomId不能为空！
     * 2003: 房间已经解散！
     * 2102: 被其他房间招人匹配覆盖！
     * 
    */
    resultErrCode?: number;

    /**匹配结果中有创建房间的结果*/
    resultCreateRoom?: IMatcherExecResultCreateRoom[];
    /**匹配结果中要加入房间的结果*/
    resultJoinRoom?: IMatcherExecResultJoinRoom[];
}

/**匹配器执行结果有创建房间的操作*/
export interface IMatcherExecResultCreateRoom {
    /**创建房间的参数*/
    createRoomPara: ICreateRoomPara;
    /**被匹配中的是哪些请求*/
    matchReqIds: string[];
    /**匹配进入这个房间的玩家id列表*/
    matchPlayerIds: string[];
    /**是否开启房间招人匹配(可能有的情况:原先就是房间招人匹配但没招满;玩家匹配满足最小人数但还没招满;)*/
    roomJoinUsMatch: boolean;
}
/**匹配器执行结果有加入房间的操作*/
export interface IMatcherExecResultJoinRoom {
    /**要加入的房间*/
    joinRoomId: string;
    /**被匹配中的是哪些请求*/
    matchReqIds: string[];
    /**匹配进入这个房间的玩家id列表*/
    matchPlayerIds: string[];
    /**要加入的那个房间,是否继续招人匹配,否的话将停止匹配*/
    roomJoinUsMatch: boolean;
}

/**匹配结果通知消息*/
export interface IMatchResultNotify {
    /**匹配请求*/
    request: IMatchRequest,

    /**匹配结果，失败的错误码*/
    result: IResult<IMatchResult>;
}

