import { IBaseVSMatcherParams } from "../../tsgf/match/Models";
import { IMatcher } from "./IMatcher";
import { MatcherRequests } from "./MatcherRequests";
import { IMatchRequest, IMatcherExecResult } from "./Models";

/**基础对战匹配器
 * IBaseVSMatchAttributes
*/
export class MatcherBaseVS implements IMatcher {
    public matcherKey: string = 'BaseVS';

    onNewMatchReq(currMatchReq: IMatchRequest, allReqs: MatcherRequests): IMatcherExecResult {
        let attr = currMatchReq.matcherParams as IBaseVSMatcherParams
        return {
            hasResult: false,
        };
    }
    onPollMatcherReqs(allReqs: MatcherRequests): IMatcherExecResult {
        return {
            hasResult: false,
        };
    }

}