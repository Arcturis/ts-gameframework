import { MatcherRequests } from "./MatcherRequests";
import { IMatcherExecResult, IMatchRequest } from "./Models";

/**匹配器接口*/
export interface IMatcher {
    /**匹配器标识*/
    matcherKey: string;

    /**
     * 当收到新的匹配请求, 这里应优先匹配快过期的
     * @date 2022/4/30 - 12:39:51
     *
     * @param { IMatchRequest[]} allReqs 同匹配器的所有匹配请求
     * @param {IMatchRequest} currMatchReq 当前匹配请求
     * @returns {IMatcherExecResult}
     */
    onNewMatchReq(currMatchReq: IMatchRequest, allReqs: IMatchRequest[]): IMatcherExecResult;

    /**
     * 同匹配器下只要还存在请求，则会定时轮询, 这里应优先匹配快过期的
     * @date 2022/4/30 - 12:39:51
     *
     * @param { IMatchRequest[]} allReqs 同匹配器的所有匹配请求
     * @returns {IMatcherExecResult}
     */
    onPollMatcherReqs(allReqs: IMatchRequest[]): IMatcherExecResult;
}