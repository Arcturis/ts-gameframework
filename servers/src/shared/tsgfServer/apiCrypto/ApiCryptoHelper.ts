import { BaseServiceType } from "tsrpc-proto";
import { CryptoHelper } from "../CryptoHelper"
import { IResult, Result } from "../../tsgf/Result";
import { IAppEncryptRequest, IAppEncryptRequestT, IBaseEncryptRequestData } from "../../tsgf/apiCrypto/Models";

export class ApiCryptoHelper {
    /**
     * 调用AppCrypto协议的加密接口
     * @date 2022/5/9 - 17:26:01
     *
     * @param {string} appId
     * @param {string} appSecret
     * @param {*} reqData
     * @returns {PIAppEncryptRequest}
     */
    public static appCryptoEncrypt(appId: string, appSecret: string, reqData: any)
        : IAppEncryptRequest {

        reqData.ts = Date.now();
        let json = JSON.stringify(reqData);
        let cText = CryptoHelper.desEncryptECB_PKCS7_Base64(json, appSecret);
        let req: IAppEncryptRequest = {
            appId: appId,
            ciphertext: cText,
        };
        return req;
    }
    /**
     * 调用AppCrypto协议的解密接口
     * @date 2022/5/9 - 17:26:01
     *
     * @param {string} appSecret
     * @param {IAppEncryptRequest} req
     * @returns {IResult<IAppEncryptRequestT<T>>}
     */
    public static appCryptoDecryption<T extends IBaseEncryptRequestData>
        (appSecret: string, appReq: IAppEncryptRequest)
        : IResult<IAppEncryptRequestT<T>> {

        if (!appReq.ciphertext) {
            return Result.buildErr("需要 ciphertext !", 4001);
        }
        try {
            let json = CryptoHelper.desDecryptECB_PKCS7_Base64(appReq.ciphertext, appSecret);
            let data = JSON.parse(json) as T;
            if (!data) {
                return Result.buildErr("接口解析失败", 4002);
            }
            if (!data.ts || data.ts + 3600000 < Date.now()) {
                return Result.buildErr("接口过期", 4002);
            }
            appReq.data = data;
            return Result.buildSucc(appReq as IAppEncryptRequestT<T>);
        } catch (err: any) {
            return Result.buildErr("接口解析失败:" + (err?.message ?? err ?? '解析失败!'), 4002);
        }

    }
}