import { v4 } from "uuid";
import { IResult, Result } from "../../tsgf/Result";
import { RedisClient } from "../redisHelper";
import { buildGuid } from "../ServerUtils";
import { IPlayerAuthInfo } from "./Models";

/**玩家认证工具, 使用本模块需要初始化*/
export class PlayerAuthHelper {

    /**需要全局初始化时设置*/
    private static getRedisClient: () => Promise<RedisClient>;
    private static openIdCheckRegex = /^[0-9a-zA-Z\-_]+$/ig;
    private static oneDaySec = 24 * 60 * 60;


    public static init(getRedisClient: () => Promise<RedisClient>) {
        PlayerAuthHelper.getRedisClient = getRedisClient;
    }

    private static buildLastPlayerTokenKey(appId: string, openId: string) {
        return `PlayerAuth:OpenIdToLastToken:appId_${appId}:openId_${openId}`;
    }
    private static buildPlayerKey(playerToken: string) {
        return `PlayerAuth:Player:token_${playerToken}`;
    }

    /**
     * 玩家认证,成功则返回playerToken
     * @date 2022/5/8 - 22:51:02
     *
     * @public
     * @static
     * @param {string} appId
     * @param {string} openId
     * @param {string} showName
     * @param {number} authTokenDay 授权有效天数
     */
    public static async authorize(appId: string, openId: string, showName: string, authTokenDay: number): Promise<IResult<IPlayerAuthInfo>> {
        if (!appId) {
            return Result.buildErr('错误的参数 appId');
        }
        if (!openId) {
            return Result.buildErr('错误的参数 playerId');
        }
        if (openId.match(PlayerAuthHelper.openIdCheckRegex)?.length === 2) {
            return Result.buildErr('参数 openId 只能由数字、字母、下划线、连接线组成');
        }
        if (!showName) {
            return Result.buildErr('错误的参数 showName');
        }
        if (authTokenDay <= 0) {
            return Result.buildErr('错误的参数 authTokenDay');
        }
        let redisClient = await PlayerAuthHelper.getRedisClient();

        let redisLastPlayerTokenKey = PlayerAuthHelper.buildLastPlayerTokenKey(appId, openId);
        let oldPToken = await redisClient.getString(redisLastPlayerTokenKey);
        if (oldPToken) {
            //存在之前授权的token
            let oldPlayerRedisKey = PlayerAuthHelper.buildPlayerKey(oldPToken);
            let oldPlayer = await redisClient.getObject<IPlayerAuthInfo>(oldPlayerRedisKey);
            if (oldPlayer) {
                oldPlayer.invalid = true;
                //重新设置进去，并且设置redis一天后过期
                await redisClient.setObject(oldPlayerRedisKey, oldPlayer, PlayerAuthHelper.oneDaySec);
            }
        }
        let playerToken = v4();
        let exSec = authTokenDay * PlayerAuthHelper.oneDaySec;
        let authInfo: IPlayerAuthInfo = {
            playerId: buildGuid("Player_"),
            appId: appId,
            openId: openId,
            showName: showName,
            playerToken: playerToken,
            invalid: false,
            expireDate: Date.now() + exSec * 1000
        };
        //redis里的多放一天
        let redisExSec = exSec + PlayerAuthHelper.oneDaySec;
        let redisPlayerKey = PlayerAuthHelper.buildPlayerKey(playerToken);
        await redisClient.setObject(redisPlayerKey, authInfo, redisExSec);
        await redisClient.setString(redisLastPlayerTokenKey, playerToken, exSec + redisExSec);
        return Result.buildSucc(authInfo);
    }


    /**
     * 身份认证，成功则返回玩家对象
     * @date 2022/5/8 - 23:06:07
     *
     * @public
     * @static
     * @param {string} playerToken
     * @returns {IResult<IPlayerAuthInfo>}
     */
    public static async verification(playerToken: string): Promise<IResult<IPlayerAuthInfo>> {
        let redisKey = PlayerAuthHelper.buildPlayerKey(playerToken);
        let player = await (await PlayerAuthHelper.getRedisClient()).getObject<IPlayerAuthInfo>(redisKey);
        if (!player || !player.playerToken) {
            return Result.buildErr('token过期或不存在！', 4001);
        }
        if (player.invalid) {
            return Result.buildErr('token已经失效！', 4002);
        }
        if (player.expireDate < Date.now()) {
            return Result.buildErr('token已经过期！', 4003);
        }
        return Result.buildSucc(player);
    }

}