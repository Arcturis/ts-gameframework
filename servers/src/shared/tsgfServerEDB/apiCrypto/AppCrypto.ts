import { ApiCryptoHelper } from "../../tsgfServer/apiCrypto/ApiCryptoHelper";
import { IAppEncryptRequest, IBaseEncryptRequest, IBaseEncryptRequestData } from "../../tsgf/apiCrypto/Models";
import { IResult, Result } from "../../tsgf/Result";
import { AppBLL, SimpleAppHelper } from "../BLL";
import { IApiCrypto } from "./IApiCrypto";


export class AppCrypto implements IApiCrypto {

    /**
     * 解密应用加密请求为原始请求对象, 成功则赋值给appReq.req
     * @date 2022/5/8 - 20:13:01
     *
     * @public
     * @static
     * @template T
     * @param {IAppEncryptReq<T>} appReq
     * @param {string} appSecret
     * @returns {IResult<T>}
     */
    public async decryptionReq<T extends IBaseEncryptRequestData>(req: IBaseEncryptRequest)
        : Promise<IResult<T>> {
        let appEnReq = req as IAppEncryptRequest;
        if (!appEnReq.appId) {
            return Result.buildErr("需要 appId !", 4001);
        }
        //let appRet = await AppBLL.Ins.selectSingle({ appId: appEnReq.appId }).waitResult();
        let appRet = await SimpleAppHelper.selectSingleByAppId(appEnReq.appId).waitResult();
        if (!appRet.succ) {
            return Result.buildErr(appRet.err, 4004);
        }
        if (!appRet.data) {
            return Result.buildErr("错误的 appId !", 4001);
        }

        let ret = ApiCryptoHelper.appCryptoDecryption(appRet.data.appSecret, appEnReq);
        if(!ret.succ){
            return Result.buildErr(ret.err, 4001);
        }

        return Result.buildSucc(ret.data.data as T);
    }
}