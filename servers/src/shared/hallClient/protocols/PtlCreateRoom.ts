import { EApiCryptoMode } from "../../tsgf/apiCrypto/Models";
import { ICreateRoomPara, ICreateRoomRsp } from "../../tsgf/room/IRoomInfo";
import { BaseRequest, BaseResponse, BaseConf } from "./base";

/**
 * 创建房间
*/
export interface ReqCreateRoom extends BaseRequest, ICreateRoomPara {
}

export interface ResCreateRoom extends BaseResponse, ICreateRoomRsp {
}

export const conf: BaseConf = {
    cryptoMode: EApiCryptoMode.None,
};