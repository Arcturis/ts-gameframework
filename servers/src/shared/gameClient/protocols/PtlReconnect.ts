import { IRoomInfo } from "../../tsgf/room/IRoomInfo";

/**
 * 断线重连
 * 
 * */
export interface ReqReconnect {
    /**之前连接上的连接ID */
    playerToken:string;
}

export interface ResReconnect {
    /**当前所在房间信息,如果没在房间中则为 null*/
    currRoomInfo: IRoomInfo | null;
}