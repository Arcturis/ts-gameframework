import { ServiceProto } from 'tsrpc-proto';
import { MsgDisconnect } from './MsgDisconnect';
import { MsgNotifyChangePlayerNetworkState } from './MsgNotifyChangePlayerNetworkState';
import { MsgNotifyDismissRoom } from './MsgNotifyDismissRoom';
import { MsgNotifyJoinRoom } from './MsgNotifyJoinRoom';
import { MsgNotifyLeaveRoom } from './MsgNotifyLeaveRoom';
import { MsgNotifyRoomMsg } from './MsgNotifyRoomMsg';
import { MsgNotifyStartFrameSync } from './MsgNotifyStartFrameSync';
import { MsgNotifyStopFrameSync } from './MsgNotifyStopFrameSync';
import { MsgNotifySyncFrame } from './MsgNotifySyncFrame';
import { MsgPlayerInpFrame } from './MsgPlayerInpFrame';
import { MsgPlayerSendSyncState } from './MsgPlayerSendSyncState';
import { MsgRequirePlayerSyncState } from './MsgRequirePlayerSyncState';
import { ReqAuthorize, ResAuthorize } from './PtlAuthorize';
import { ReqDismissRoom, ResDismissRoom } from './PtlDismissRoom';
import { ReqJoinRoom, ResJoinRoom } from './PtlJoinRoom';
import { ReqLeaveRoom, ResLeaveRoom } from './PtlLeaveRoom';
import { ReqReconnect, ResReconnect } from './PtlReconnect';
import { ReqRequestAfterFrames, ResRequestAfterFrames } from './PtlRequestAfterFrames';
import { ReqRequestFrames, ResRequestFrames } from './PtlRequestFrames';
import { ReqSendRoomMsg, ResSendRoomMsg } from './PtlSendRoomMsg';
import { ReqStartFrameSync, ResStartFrameSync } from './PtlStartFrameSync';
import { ReqStopFrameSync, ResStopFrameSync } from './PtlStopFrameSync';

export interface ServiceType {
    api: {
        "Authorize": {
            req: ReqAuthorize,
            res: ResAuthorize
        },
        "DismissRoom": {
            req: ReqDismissRoom,
            res: ResDismissRoom
        },
        "JoinRoom": {
            req: ReqJoinRoom,
            res: ResJoinRoom
        },
        "LeaveRoom": {
            req: ReqLeaveRoom,
            res: ResLeaveRoom
        },
        "Reconnect": {
            req: ReqReconnect,
            res: ResReconnect
        },
        "RequestAfterFrames": {
            req: ReqRequestAfterFrames,
            res: ResRequestAfterFrames
        },
        "RequestFrames": {
            req: ReqRequestFrames,
            res: ResRequestFrames
        },
        "SendRoomMsg": {
            req: ReqSendRoomMsg,
            res: ResSendRoomMsg
        },
        "StartFrameSync": {
            req: ReqStartFrameSync,
            res: ResStartFrameSync
        },
        "StopFrameSync": {
            req: ReqStopFrameSync,
            res: ResStopFrameSync
        }
    },
    msg: {
        "Disconnect": MsgDisconnect,
        "NotifyChangePlayerNetworkState": MsgNotifyChangePlayerNetworkState,
        "NotifyDismissRoom": MsgNotifyDismissRoom,
        "NotifyJoinRoom": MsgNotifyJoinRoom,
        "NotifyLeaveRoom": MsgNotifyLeaveRoom,
        "NotifyRoomMsg": MsgNotifyRoomMsg,
        "NotifyStartFrameSync": MsgNotifyStartFrameSync,
        "NotifyStopFrameSync": MsgNotifyStopFrameSync,
        "NotifySyncFrame": MsgNotifySyncFrame,
        "PlayerInpFrame": MsgPlayerInpFrame,
        "PlayerSendSyncState": MsgPlayerSendSyncState,
        "RequirePlayerSyncState": MsgRequirePlayerSyncState
    }
}

export const serviceProto: ServiceProto<ServiceType> = {
    "version": 32,
    "services": [
        {
            "id": 13,
            "name": "Disconnect",
            "type": "msg"
        },
        {
            "id": 33,
            "name": "NotifyChangePlayerNetworkState",
            "type": "msg"
        },
        {
            "id": 16,
            "name": "NotifyDismissRoom",
            "type": "msg"
        },
        {
            "id": 17,
            "name": "NotifyJoinRoom",
            "type": "msg"
        },
        {
            "id": 18,
            "name": "NotifyLeaveRoom",
            "type": "msg"
        },
        {
            "id": 19,
            "name": "NotifyRoomMsg",
            "type": "msg"
        },
        {
            "id": 23,
            "name": "NotifyStartFrameSync",
            "type": "msg"
        },
        {
            "id": 24,
            "name": "NotifyStopFrameSync",
            "type": "msg"
        },
        {
            "id": 25,
            "name": "NotifySyncFrame",
            "type": "msg"
        },
        {
            "id": 26,
            "name": "PlayerInpFrame",
            "type": "msg"
        },
        {
            "id": 27,
            "name": "PlayerSendSyncState",
            "type": "msg"
        },
        {
            "id": 28,
            "name": "RequirePlayerSyncState",
            "type": "msg"
        },
        {
            "id": 14,
            "name": "Authorize",
            "type": "api"
        },
        {
            "id": 20,
            "name": "DismissRoom",
            "type": "api"
        },
        {
            "id": 15,
            "name": "JoinRoom",
            "type": "api"
        },
        {
            "id": 21,
            "name": "LeaveRoom",
            "type": "api"
        },
        {
            "id": 6,
            "name": "Reconnect",
            "type": "api"
        },
        {
            "id": 32,
            "name": "RequestAfterFrames",
            "type": "api"
        },
        {
            "id": 29,
            "name": "RequestFrames",
            "type": "api"
        },
        {
            "id": 22,
            "name": "SendRoomMsg",
            "type": "api"
        },
        {
            "id": 30,
            "name": "StartFrameSync",
            "type": "api"
        },
        {
            "id": 31,
            "name": "StopFrameSync",
            "type": "api"
        }
    ],
    "types": {
        "MsgDisconnect/MsgDisconnect": {
            "type": "Interface"
        },
        "MsgNotifyChangePlayerNetworkState/MsgNotifyChangePlayerNetworkState": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "roomInfo",
                    "type": {
                        "type": "Reference",
                        "target": "../../tsgf/room/IRoomInfo/IRoomInfo"
                    }
                },
                {
                    "id": 1,
                    "name": "changePlayerId",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 2,
                    "name": "networkState",
                    "type": {
                        "type": "Reference",
                        "target": "../../tsgf/player/IPlayerInfo/ENetworkState"
                    }
                }
            ]
        },
        "../../tsgf/room/IRoomInfo/IRoomInfo": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "roomId",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "roomName",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 2,
                    "name": "ownerPlayerId",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 3,
                    "name": "isPrivate",
                    "type": {
                        "type": "Boolean"
                    }
                },
                {
                    "id": 14,
                    "name": "matcherKey",
                    "type": {
                        "type": "String"
                    },
                    "optional": true
                },
                {
                    "id": 4,
                    "name": "isForbidJoin",
                    "type": {
                        "type": "Boolean"
                    }
                },
                {
                    "id": 5,
                    "name": "createType",
                    "type": {
                        "type": "Reference",
                        "target": "../../tsgf/room/IRoomInfo/ERoomCreateType"
                    }
                },
                {
                    "id": 6,
                    "name": "maxPlayers",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 7,
                    "name": "roomType",
                    "type": {
                        "type": "String"
                    },
                    "optional": true
                },
                {
                    "id": 8,
                    "name": "customProperties",
                    "type": {
                        "type": "String"
                    },
                    "optional": true
                },
                {
                    "id": 9,
                    "name": "playerList",
                    "type": {
                        "type": "Array",
                        "elementType": {
                            "type": "Reference",
                            "target": "../../tsgf/player/IPlayerInfo/IPlayerInfo"
                        }
                    }
                },
                {
                    "id": 10,
                    "name": "frameRate",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 11,
                    "name": "frameSyncState",
                    "type": {
                        "type": "Reference",
                        "target": "../../tsgf/room/IRoomInfo/EFrameSyncState"
                    }
                },
                {
                    "id": 12,
                    "name": "createTime",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 13,
                    "name": "startGameTime",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "../../tsgf/room/IRoomInfo/ERoomCreateType": {
            "type": "Enum",
            "members": [
                {
                    "id": 0,
                    "value": 0
                },
                {
                    "id": 1,
                    "value": 1
                }
            ]
        },
        "../../tsgf/player/IPlayerInfo/IPlayerInfo": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "playerId",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "showName",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 2,
                    "name": "teamId",
                    "type": {
                        "type": "String"
                    },
                    "optional": true
                },
                {
                    "id": 3,
                    "name": "customPlayerStatus",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 4,
                    "name": "customProfile",
                    "type": {
                        "type": "String"
                    },
                    "optional": true
                },
                {
                    "id": 8,
                    "name": "networkState",
                    "type": {
                        "type": "Reference",
                        "target": "../../tsgf/player/IPlayerInfo/ENetworkState"
                    }
                },
                {
                    "id": 7,
                    "name": "isRobot",
                    "type": {
                        "type": "Boolean"
                    }
                }
            ]
        },
        "../../tsgf/player/IPlayerInfo/ENetworkState": {
            "type": "Enum",
            "members": [
                {
                    "id": 0,
                    "value": 0
                },
                {
                    "id": 1,
                    "value": 1
                }
            ]
        },
        "../../tsgf/room/IRoomInfo/EFrameSyncState": {
            "type": "Enum",
            "members": [
                {
                    "id": 0,
                    "value": 0
                },
                {
                    "id": 1,
                    "value": 1
                }
            ]
        },
        "MsgNotifyDismissRoom/MsgNotifyDismissRoom": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "roomInfo",
                    "type": {
                        "type": "Reference",
                        "target": "../../tsgf/room/IRoomInfo/IRoomInfo"
                    }
                }
            ]
        },
        "MsgNotifyJoinRoom/MsgNotifyJoinRoom": {
            "type": "Interface",
            "properties": [
                {
                    "id": 1,
                    "name": "roomInfo",
                    "type": {
                        "type": "Reference",
                        "target": "../../tsgf/room/IRoomInfo/IRoomInfo"
                    }
                },
                {
                    "id": 2,
                    "name": "joinPlayerId",
                    "type": {
                        "type": "String"
                    }
                }
            ]
        },
        "MsgNotifyLeaveRoom/MsgNotifyLeaveRoom": {
            "type": "Interface",
            "properties": [
                {
                    "id": 2,
                    "name": "leavePlayerInfo",
                    "type": {
                        "type": "Reference",
                        "target": "../../tsgf/player/IPlayerInfo/IPlayerInfo"
                    }
                },
                {
                    "id": 1,
                    "name": "roomInfo",
                    "type": {
                        "type": "Reference",
                        "target": "../../tsgf/room/IRoomInfo/IRoomInfo"
                    }
                }
            ]
        },
        "MsgNotifyRoomMsg/MsgNotifyRoomMsg": {
            "type": "Interface",
            "properties": [
                {
                    "id": 3,
                    "name": "recvRoomMsg",
                    "type": {
                        "type": "Reference",
                        "target": "../../../shared/tsgf/room/IRoomMsg/IRecvRoomMsg"
                    }
                }
            ]
        },
        "../../../shared/tsgf/room/IRoomMsg/IRecvRoomMsg": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "fromPlayerInfo",
                    "type": {
                        "type": "Reference",
                        "target": "../../../shared/tsgf/player/IPlayerInfo/IPlayerInfo"
                    }
                },
                {
                    "id": 1,
                    "name": "msg",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 2,
                    "name": "recvType",
                    "type": {
                        "type": "Reference",
                        "target": "../../../shared/tsgf/room/IRoomMsg/ERoomMsgRecvType"
                    }
                }
            ]
        },
        "../../../shared/tsgf/player/IPlayerInfo/IPlayerInfo": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "playerId",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "showName",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 2,
                    "name": "teamId",
                    "type": {
                        "type": "String"
                    },
                    "optional": true
                },
                {
                    "id": 3,
                    "name": "customPlayerStatus",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 4,
                    "name": "customProfile",
                    "type": {
                        "type": "String"
                    },
                    "optional": true
                },
                {
                    "id": 8,
                    "name": "networkState",
                    "type": {
                        "type": "Reference",
                        "target": "../../../shared/tsgf/player/IPlayerInfo/ENetworkState"
                    }
                },
                {
                    "id": 7,
                    "name": "isRobot",
                    "type": {
                        "type": "Boolean"
                    }
                }
            ]
        },
        "../../../shared/tsgf/player/IPlayerInfo/ENetworkState": {
            "type": "Enum",
            "members": [
                {
                    "id": 0,
                    "value": 0
                },
                {
                    "id": 1,
                    "value": 1
                }
            ]
        },
        "../../../shared/tsgf/room/IRoomMsg/ERoomMsgRecvType": {
            "type": "Enum",
            "members": [
                {
                    "id": 0,
                    "value": 1
                },
                {
                    "id": 1,
                    "value": 2
                },
                {
                    "id": 2,
                    "value": 3
                }
            ]
        },
        "MsgNotifyStartFrameSync/MsgNotifyStartFrameSync": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "startPlayerId",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "roomInfo",
                    "type": {
                        "type": "Reference",
                        "target": "../../tsgf/room/IRoomInfo/IRoomInfo"
                    }
                }
            ]
        },
        "MsgNotifyStopFrameSync/MsgNotifyStopFrameSync": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "stopPlayerId",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "roomInfo",
                    "type": {
                        "type": "Reference",
                        "target": "../../tsgf/room/IRoomInfo/IRoomInfo"
                    }
                }
            ]
        },
        "MsgNotifySyncFrame/MsgNotifySyncFrame": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "syncFrame",
                    "type": {
                        "type": "Reference",
                        "target": "../../tsgf/room/IGameFrame/IGameSyncFrame"
                    }
                }
            ]
        },
        "../../tsgf/room/IGameFrame/IGameSyncFrame": {
            "type": "Interface",
            "properties": [
                {
                    "id": 1,
                    "name": "frameIndex",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 0,
                    "name": "playerInputs",
                    "type": {
                        "type": "Union",
                        "members": [
                            {
                                "id": 0,
                                "type": {
                                    "type": "Array",
                                    "elementType": {
                                        "type": "Reference",
                                        "target": "../../tsgf/room/IGameFrame/IFramePlayerInput"
                                    }
                                }
                            },
                            {
                                "id": 1,
                                "type": {
                                    "type": "Literal",
                                    "literal": null
                                }
                            }
                        ]
                    }
                }
            ],
            "indexSignature": {
                "keyType": "String",
                "type": {
                    "type": "Any"
                }
            }
        },
        "../../tsgf/room/IGameFrame/IFramePlayerInput": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "playerId",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "inputFrameType",
                    "type": {
                        "type": "Reference",
                        "target": "../../tsgf/room/IGameFrame/EPlayerInputFrameType"
                    }
                },
                {
                    "id": 2,
                    "name": "operates",
                    "type": {
                        "type": "Array",
                        "elementType": {
                            "type": "Reference",
                            "target": "../../tsgf/room/IGameFrame/IPlayerInputOperate"
                        }
                    },
                    "optional": true
                }
            ],
            "indexSignature": {
                "keyType": "String",
                "type": {
                    "type": "Any"
                }
            }
        },
        "../../tsgf/room/IGameFrame/EPlayerInputFrameType": {
            "type": "Enum",
            "members": [
                {
                    "id": 0,
                    "value": 1
                },
                {
                    "id": 1,
                    "value": 2
                },
                {
                    "id": 2,
                    "value": 3
                },
                {
                    "id": 3,
                    "value": 4
                }
            ]
        },
        "../../tsgf/room/IGameFrame/IPlayerInputOperate": {
            "type": "Interface",
            "indexSignature": {
                "keyType": "String",
                "type": {
                    "type": "Any"
                }
            }
        },
        "MsgPlayerInpFrame/MsgPlayerInpFrame": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "operates",
                    "type": {
                        "type": "Array",
                        "elementType": {
                            "type": "Reference",
                            "target": "../../tsgf/room/IGameFrame/IPlayerInputOperate"
                        }
                    }
                }
            ]
        },
        "MsgPlayerSendSyncState/MsgPlayerSendSyncState": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "stateData",
                    "type": {
                        "type": "Any"
                    }
                },
                {
                    "id": 1,
                    "name": "stateFrameIndex",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "MsgRequirePlayerSyncState/MsgRequirePlayerSyncState": {
            "type": "Interface"
        },
        "PtlAuthorize/ReqAuthorize": {
            "type": "Interface",
            "extends": [
                {
                    "id": 2,
                    "type": {
                        "type": "Reference",
                        "target": "../../tsgf/player/IPlayerInfo/IPlayerAuthPara"
                    }
                }
            ],
            "properties": [
                {
                    "id": 0,
                    "name": "playerToken",
                    "type": {
                        "type": "String"
                    }
                }
            ]
        },
        "../../tsgf/player/IPlayerInfo/IPlayerAuthPara": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "customPlayerStatus",
                    "type": {
                        "type": "Number"
                    },
                    "optional": true
                },
                {
                    "id": 1,
                    "name": "customProfile",
                    "type": {
                        "type": "String"
                    },
                    "optional": true
                },
                {
                    "id": 2,
                    "name": "isRobot",
                    "type": {
                        "type": "Boolean"
                    },
                    "optional": true
                }
            ]
        },
        "PtlAuthorize/ResAuthorize": {
            "type": "Interface",
            "properties": [
                {
                    "id": 1,
                    "name": "playerInfo",
                    "type": {
                        "type": "Reference",
                        "target": "../../tsgf/player/IPlayerInfo/IPlayerInfo"
                    }
                }
            ]
        },
        "PtlDismissRoom/ReqDismissRoom": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "roomId",
                    "type": {
                        "type": "String"
                    }
                }
            ]
        },
        "PtlDismissRoom/ResDismissRoom": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "roomInfo",
                    "type": {
                        "type": "Reference",
                        "target": "../../tsgf/room/IRoomInfo/IRoomInfo"
                    }
                }
            ]
        },
        "PtlJoinRoom/ReqJoinRoom": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "roomId",
                    "type": {
                        "type": "String"
                    }
                }
            ]
        },
        "PtlJoinRoom/ResJoinRoom": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "roomInfo",
                    "type": {
                        "type": "Reference",
                        "target": "../../tsgf/room/IRoomInfo/IRoomInfo"
                    }
                }
            ]
        },
        "PtlLeaveRoom/ReqLeaveRoom": {
            "type": "Interface"
        },
        "PtlLeaveRoom/ResLeaveRoom": {
            "type": "Interface"
        },
        "PtlReconnect/ReqReconnect": {
            "type": "Interface",
            "properties": [
                {
                    "id": 2,
                    "name": "playerToken",
                    "type": {
                        "type": "String"
                    }
                }
            ]
        },
        "PtlReconnect/ResReconnect": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "currRoomInfo",
                    "type": {
                        "type": "Union",
                        "members": [
                            {
                                "id": 0,
                                "type": {
                                    "type": "Reference",
                                    "target": "../../tsgf/room/IRoomInfo/IRoomInfo"
                                }
                            },
                            {
                                "id": 1,
                                "type": {
                                    "type": "Literal",
                                    "literal": null
                                }
                            }
                        ]
                    }
                }
            ]
        },
        "PtlRequestAfterFrames/ReqRequestAfterFrames": {
            "type": "Interface"
        },
        "PtlRequestAfterFrames/ResRequestAfterFrames": {
            "type": "Interface",
            "extends": [
                {
                    "id": 0,
                    "type": {
                        "type": "Reference",
                        "target": "../../tsgf/room/IGameFrame/IAfterFrames"
                    }
                }
            ]
        },
        "../../tsgf/room/IGameFrame/IAfterFrames": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "stateData",
                    "type": {
                        "type": "Any"
                    }
                },
                {
                    "id": 1,
                    "name": "stateFrameIndex",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 2,
                    "name": "afterFrames",
                    "type": {
                        "type": "Array",
                        "elementType": {
                            "type": "Reference",
                            "target": "../../tsgf/room/IGameFrame/IGameSyncFrame"
                        }
                    }
                },
                {
                    "id": 3,
                    "name": "maxSyncFrameIndex",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 4,
                    "name": "serverSyncFrameRate",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "PtlRequestFrames/ReqRequestFrames": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "beginFrameIndex",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 1,
                    "name": "endFrameIndex",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "PtlRequestFrames/ResRequestFrames": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "frames",
                    "type": {
                        "type": "Array",
                        "elementType": {
                            "type": "Reference",
                            "target": "../../tsgf/room/IGameFrame/IGameSyncFrame"
                        }
                    }
                }
            ]
        },
        "PtlSendRoomMsg/ReqSendRoomMsg": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "roomMsg",
                    "type": {
                        "type": "Reference",
                        "target": "../../../shared/tsgf/room/IRoomMsg/IRoomMsg"
                    }
                }
            ]
        },
        "../../../shared/tsgf/room/IRoomMsg/IRoomMsg": {
            "type": "Union",
            "members": [
                {
                    "id": 0,
                    "type": {
                        "type": "Reference",
                        "target": "../../../shared/tsgf/room/IRoomMsg/IRoomMsgOtherPlayers"
                    }
                },
                {
                    "id": 2,
                    "type": {
                        "type": "Reference",
                        "target": "../../../shared/tsgf/room/IRoomMsg/IRoomMsgSomePlayers"
                    }
                }
            ]
        },
        "../../../shared/tsgf/room/IRoomMsg/IRoomMsgOtherPlayers": {
            "type": "Interface",
            "extends": [
                {
                    "id": 0,
                    "type": {
                        "type": "Reference",
                        "target": "../../../shared/tsgf/room/IRoomMsg/IRoomMsgBase"
                    }
                }
            ],
            "properties": [
                {
                    "id": 0,
                    "name": "recvType",
                    "type": {
                        "type": "Union",
                        "members": [
                            {
                                "id": 0,
                                "type": {
                                    "type": "Literal",
                                    "literal": 1
                                }
                            },
                            {
                                "id": 1,
                                "type": {
                                    "type": "Literal",
                                    "literal": 2
                                }
                            }
                        ]
                    }
                }
            ]
        },
        "../../../shared/tsgf/room/IRoomMsg/IRoomMsgBase": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "msg",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "recvType",
                    "type": {
                        "type": "Reference",
                        "target": "../../../shared/tsgf/room/IRoomMsg/ERoomMsgRecvType"
                    }
                }
            ]
        },
        "../../../shared/tsgf/room/IRoomMsg/IRoomMsgSomePlayers": {
            "type": "Interface",
            "extends": [
                {
                    "id": 0,
                    "type": {
                        "type": "Reference",
                        "target": "../../../shared/tsgf/room/IRoomMsg/IRoomMsgBase"
                    }
                }
            ],
            "properties": [
                {
                    "id": 0,
                    "name": "recvType",
                    "type": {
                        "type": "Literal",
                        "literal": 3
                    }
                },
                {
                    "id": 1,
                    "name": "recvPlayerList",
                    "type": {
                        "type": "Array",
                        "elementType": {
                            "type": "String"
                        }
                    }
                }
            ]
        },
        "PtlSendRoomMsg/ResSendRoomMsg": {
            "type": "Interface"
        },
        "PtlStartFrameSync/ReqStartFrameSync": {
            "type": "Interface"
        },
        "PtlStartFrameSync/ResStartFrameSync": {
            "type": "Interface"
        },
        "PtlStopFrameSync/ReqStopFrameSync": {
            "type": "Interface"
        },
        "PtlStopFrameSync/ResStopFrameSync": {
            "type": "Interface"
        }
    }
};