import { IRecvRoomMsg } from "../../../shared/tsgf/room/IRoomMsg";

export interface MsgNotifyRoomMsg {
    recvRoomMsg: IRecvRoomMsg;
}
