
function formatObj(obj: any) {
    if (obj.stack) {
        return obj.stack;
    } else if (typeof (obj) === 'object') {
        return JSON.stringify(obj, null, 4);
    }
    return obj;
}
function objArrJoin(arr: any[]) {
    let str = "", sp = "";
    for (let i = 0; i < arr.length; i++) {
        str += sp;
        str += formatObj(arr[i]);
        sp = " \n ";
    }
    return str
}

export const logger = {
    debug(...args: any[]) {
        // 什么也不做，相当于隐藏了日志
    },
    log(...args: any[]) {
        if (!args || args.length <= 0) return;
        // 让日志仍然输出到控制台
        if (args.find(
            a => a
                && a.indexOf
                && (
                    a.indexOf("SyncFrame") > -1
                    || a.indexOf("ClusterSyncNodeInfo") > -1
                    || a.indexOf("InpFrame") > -1
                    || a.indexOf("AfterFrames") > -1
                    || a.indexOf("SyncState") > -1
                )
        )
        ) {
            //消息太频繁不输出
            return;
        }
        console.log(objArrJoin(args));
        //console.log(...args);
    },
    warn(...args: any[]) {
        if (!args || args.length <= 0) return;
        console.warn(objArrJoin(args));
        //console.warn(...args);
    },
    error(...args: any[]) {
        if (!args || args.length <= 0) return;
        console.error(objArrJoin(args));
        //console.error(...args);
    },
}