import { IResult } from "./Result";


/**可取消对象*/
export interface ICancelable {
    
    /**
     * 取消执行
     * @date 2022/4/25 - 18:27:48
     *
     * @returns {Promise<void>}
     */
    cancel(): Promise<void>;
}


/**可取消的执行对象*/
export interface ICancelableExec<ResultData> extends ICancelable {
    /**
     * 等待执行结果
     * @date 2022/4/25 - 17:58:03
     *
     * @returns {Promise<IResult<ResultData>>}
     */
    waitResult(): Promise<IResult<ResultData>>;
}