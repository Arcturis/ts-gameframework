


/**通用结果对象*/
export type IResult<T> = {
    /**结果是成功的*/
    succ: true,
    /**结果代码*/
    code: number,
    /**如果是失败的，则有错误消息*/
    err?: string,
    data: T,
} | {
    succ: false,
    /**结果代码*/
    code: number,
    /**如果是失败的，则有错误消息*/
    err: string,
    data?: T,
};

export class Result<T> {

    /**
     * 构建一个错误的结果对象
     * @date 2022/4/23 - 01:08:16
     *
     * @public
     * @static
     * @template T
     * @param {string} errMsg
     * @param {number} [code=0]
     * @returns {IResult<T>}
     */
    public static buildErr<T>(errMsg: string, code: number = 1): IResult<T> {
        return {
            succ: false,
            err: errMsg,
            code: code,
        };
    }

    /**
     * 构建一个成功的结果对象
     * @date 2022/4/23 - 01:08:23
     *
     * @public
     * @static
     * @template T
     * @param {T} data
     * @returns {IResult<T>}
     */
    public static buildSucc<T>(data: T): IResult<T> {
        return {
            succ: true,
            code: 0,
            data: data,
        };
    }
}
