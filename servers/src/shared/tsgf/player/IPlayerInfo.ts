

export enum ENetworkState {
    OFFLINE = 0,
    ONLINE = 1,
}

export interface IPlayerInfo {
    /**玩家ID*/
    playerId: string;
    /**显示名*/
    showName: string;
    /**当前所在队伍id*/
    teamId?: string;
    /**自定义玩家状态*/
    customPlayerStatus: number;
    /**自定义玩家信息*/
    customProfile?: string;
    /**网络状态*/
    networkState: ENetworkState;
    /**是否机器人*/
    isRobot: boolean;
}

/**玩家认证参数*/
export interface IPlayerAuthPara{
    customPlayerStatus?: number;
    customProfile?: string;
    isRobot?: boolean;
}