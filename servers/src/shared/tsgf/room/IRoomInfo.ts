import { IPlayerInfo } from "../player/IPlayerInfo";

/**创建房间的方式*/
export enum ERoomCreateType {
    /**调用创建房间方法创建的*/
    COMMON_CREATE = 0,
    /**由匹配创建的*/
    MATCH_CREATE = 1,
}

/**帧同步状态*/
export enum EFrameSyncState {
    /**未开始帧同步*/
    STOP = 0,
    /**已开始帧同步*/
    START = 1,
}

export interface IRoomInfo {
    /**房间ID*/
    roomId: string;
    /**房间名称*/
    roomName: string;
    /**房主玩家ID，创建后，只有房主玩家的客户端才可以调用相关的管理操作*/
    ownerPlayerId: string;
    /**是否私有房间，即不参与匹配, 但可以通过房间ID加入*/
    isPrivate: boolean;
    /**如果参与匹配,则使用的匹配器标识*/
    matcherKey?: string;
    /**是否不允许加人*/
    isForbidJoin: boolean;
    /**创建房间的方式*/
    createType: ERoomCreateType;
    /**进入房间的最大玩家数量*/
    maxPlayers: number;
    /**房间类型字符串*/
    roomType?: string;
    /**自定义房间属性字符串*/
    customProperties?: string;

    /**玩家列表*/
    playerList: IPlayerInfo[];

    /**帧率*/
    frameRate: number;
    /**帧同步状态*/
    frameSyncState: EFrameSyncState;
    /**创建房间时间戳（单位毫秒， new Date(createTime) 可获得时间对象）*/
    createTime: number;
    /**开始游戏时间戳（单位毫秒， new Date(createTime) 可获得时间对象）,0表示未开始*/
    startGameTime: number;
}

/**创建房间的参数*/
export interface ICreateRoomPara {
    /**房间名字，查询房间和加入房间时会获取到*/
    roomName: string;
    /**房主玩家ID，创建后，只有房主玩家的客户端才可以调用相关的管理操作, 如果不想任何人操作,可以直接设置为'', 所有人都离开房间后自动解散*/
    ownerPlayerId: string;
    /**进入房间的最大玩家数量*/
    maxPlayers: number;
    /**是否私有房间，即不参与匹配, 但可以通过房间ID加入*/
    isPrivate: boolean;
    /**如果参与匹配,则使用的匹配器标识*/
    matcherKey?: string;
    /**自定义房间属性字符串*/
    customProperties?: string;
    /**房间类型，查询房间信息时可以获取到*/
    roomType?: string;
}
/**创建房间的响应*/
export interface ICreateRoomRsp {
    /**游戏服务器的连接地址*/
    gameServerUrl: string;
    /**房间信息*/
    roomInfo: IRoomInfo;
}

export interface IRoomRegInfo {
    /**游戏服务器的连接地址*/
    gameServerUrl: string;
}