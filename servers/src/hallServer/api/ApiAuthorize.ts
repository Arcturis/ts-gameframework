import { ApiCall } from "tsrpc";
import { Result } from "../../shared/tsgf/Result";
import { PlayerAuthHelper } from "../../shared/tsgfServer/auth/PlayerAuthHelper";
import { ReqAuthorize, ReqAuthorizeT, ResAuthorize } from "../../shared/hallClient/protocols/PtlAuthorize";
import { HallApiCall } from "../HallServer";

export async function ApiAuthorize(call: HallApiCall<ReqAuthorizeT, ResAuthorize>) {
    let reqData = call.req.data;
    if (reqData.authTokenDay > 120) {
        return call.error('authTokenDay 范围1~120！', { code: 1001 });
    }
    let ret = await PlayerAuthHelper.authorize(call.req.appId, reqData.openId, reqData.showName, reqData.authTokenDay);
    if (!ret.succ) {
        return call.error(ret.err, { code: ret.code });
    }
    call.succ({
        playerId: ret.data.playerId,
        playerToken: ret.data.playerToken,
    });
}