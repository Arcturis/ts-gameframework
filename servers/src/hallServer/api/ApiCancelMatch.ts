
import { ReqCancelMatch, ResCancelMatch } from "../../shared/hallClient/protocols/PtlCancelMatch";
import { HallApiCall } from "../HallServer";

export async function ApiCancelMatch(call: HallApiCall<ReqCancelMatch, ResCancelMatch>) {
    let ret = await call.getHallServer().matchRequestServer
        .cancelMatch(call.conn.currPlayer.authInfo.appId, call.req.matchReqId, call.conn.currPlayer.playerInfo.playerId);
    if (!ret.succ) {
        return call.error(ret.err, { code: ret.code });
    }
    return call.succ({});
}