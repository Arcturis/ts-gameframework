import { ReqRequestMatch, ResRequestMatch } from "../../shared/hallClient/protocols/PtlRequestMatch";
import { HallApiCall } from "../HallServer";

export async function ApiRequestMatch(call: HallApiCall<ReqRequestMatch, ResRequestMatch>) {
    let ret = await call.getHallServer().matchRequestServer
        .requestMatch(call.conn.currPlayer.authInfo.appId, call.req.matchParams);
    if (!ret.succ) {
        return call.error(ret.err, { code: ret.code });
    }
    return call.succ({
        matchReqId: ret.data
    });
}