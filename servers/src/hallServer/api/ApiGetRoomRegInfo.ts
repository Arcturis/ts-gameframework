import { RoomHelper } from "../../shared/tsgfServer/room/RoomHelper";
import { ReqGetRoomRegInfo, ResGetRoomRegInfo } from "../../shared/hallClient/protocols/PtlGetRoomRegInfo";
import { HallApiCall } from "../HallServer";

export async function ApiGetRoomRegInfo(call: HallApiCall<ReqGetRoomRegInfo, ResGetRoomRegInfo>) {
    let regInfo = await RoomHelper.getRoomRegInfo(call.req.roomId);
    if (!regInfo) {
        return call.error('房间不存在！', { code: 2002 });
    }
    let gameServer = await call.getHallServer().getGameServer(regInfo.gameServerNodeId);
    if (!gameServer) {
        return call.error('房间服务器已经关闭，请重新创建房间！', { code: 2003 });
    }
    call.succ({
        regInfo: {
            gameServerUrl: gameServer.serverUrl,
        }
    });
}