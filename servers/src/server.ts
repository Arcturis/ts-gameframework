
import * as path from "path";
/*
当前加载了哪些服务，由本文件决定
*/
import { getServerConfig, getServerRedisClient, RunServerKey } from "./serverConfigMgr";
import { logger } from "./shared/tsgf/logger";
import { IPlayer } from "./shared/tsgfServer/auth/Models";

import { HallServer } from "./hallServer/HallServer";
import { GameServerClusterMgr } from "./gameServerCluster/GameServerClusterMgr";
import { MatchServerClusterMgr } from "./matchServerCluster/MatchServerClusterMgr";
import { GameServer } from "./gameServer/GameServer";
import { MatchServer } from "./matchServer/MatchServer";
import { HttpConnection, HttpServer, ServiceProto } from "tsrpc";

import { serviceProto as demoServiceProto, ServiceType as DemoServiceType } from "./shared/demoClient/protocols/serviceProto";

let hallServer: HallServer | null = null;
let gameServerClusterMgr: GameServerClusterMgr | null = null;
let matchServerClusterMgr: MatchServerClusterMgr | null = null;
let gameServer: GameServer | null = null;
let matchServer: MatchServer | null = null;

let demoServer: HttpServer | null = null;

/**拓展字段*/
declare module 'tsrpc' {
    export interface BaseConnection {
        /**连接ID,连接在服务端唯一标识*/
        connectionId: string;
        /**玩家ID, 只要通过认证都不会为空, 并且即使销毁, 这个字段值还在*/
        playerId: string;
        /**当前连接所属的玩家服务器对象, 只要通过认证都不会为空, 但断开连接后对象会销毁!*/
        currPlayer: IPlayer;
    }
}


async function startHallServer() {
    // 大厅服务器
    logger?.log("大厅服务器.port:", getServerConfig().hallServer.port);
    hallServer = new HallServer(
        getServerRedisClient,
        {
            port: getServerConfig().hallServer.port,
            json: true,
            logger: logger,
        }
    );
    await hallServer.start();
    logger.log("大厅服务启动成功!");
}

async function startGameServerClusterMgr() {

    // 游戏服务器集群管理服务（可选，即可另外独立部署、启动）
    logger?.log("gameClusterServer.port:", getServerConfig().gameServerCluster.port);
    gameServerClusterMgr = new GameServerClusterMgr(
        () => {
            //每次都从配置中读取
            return getServerConfig().gameServerCluster.nodeList;
        },
        {
            port: getServerConfig().gameServerCluster.port,
            json: false,
            logger: logger,
        },
        async (reuseClient: boolean = true) => await getServerRedisClient(reuseClient)
    );
    await gameServerClusterMgr.start();
    logger.log("游戏集群管理服务启动成功!");
}

async function startMatchServerClusterMgr() {
    // 匹配服务器集群管理服务（可选，即可另外独立部署、启动）
    logger?.log("matchClusterServer.port:", getServerConfig().matchServerCluster.port);
    matchServerClusterMgr = new MatchServerClusterMgr(
        () => {
            //每次都从配置中读取
            return getServerConfig().matchServerCluster.nodeList;
        },
        {
            port: getServerConfig().matchServerCluster.port,
            json: false,
            logger: logger,
        },
        async (reuseClient: boolean = true) => await getServerRedisClient(reuseClient)
    );
    await matchServerClusterMgr.start();
    logger.log("匹配集群管理服务启动成功!");
}

async function startGameServer() {
    let serverCfg = getServerConfig();
    logger?.log("gameServer: port:", serverCfg.gameServer.listenPort);
    gameServer = new GameServer(getServerRedisClient, () => getServerConfig().gameServer);
    await gameServer.start();
    logger.log("游戏服务启动成功!");
}

async function startMatchServer() {
    let serverCfg = getServerConfig();
    logger?.log("matchServer:", serverCfg.matchServer.clusterNodeId, serverCfg.matchServer.serverName);
    matchServer = new MatchServer(
        serverCfg.matchServer.clusterWSUrl,
        serverCfg.matchServer.clusterNodeId,
        serverCfg.matchServer.clusterKey,
        getServerRedisClient);
    await matchServer.start();
    logger.log("匹配处理服务启动成功!");
}

async function startDemoServer() {
    demoServer = new HttpServer<DemoServiceType>(demoServiceProto, {
        port: 7901,
        json: true,
        logger: logger,
    });
    demoServer.flows.preRecvDataFlow.push(v => {
        let conn = v.conn as HttpConnection;
        //解决HTTP请求跨域问题
        conn.httpRes.setHeader("Access-Control-Allow-Origin", "*");
        return v;
    })
    await demoServer.autoImplementApi(path.resolve(__dirname, 'demoServer/api'));
    await demoServer.start();
    logger.log("示例应用的用户系统模拟服务启动成功!");
}


/**启动当前选用的所有服务*/
export async function startServers() {
    let rs = getServerConfig().runServer;
    for (let i = 0; i < rs.length; i++) {
        let r = rs[i];
        switch (r) {
            case RunServerKey.HallServer:
                await startHallServer();
                break;
            case RunServerKey.GameServerCluster:
                await startGameServerClusterMgr();
                break;
            case RunServerKey.MatchServerCluster:
                await startMatchServerClusterMgr();
                break;
            case RunServerKey.GameServer:
                await startGameServer();
                break;
            case RunServerKey.MatchServer:
                await startMatchServer();
                break;
            case RunServerKey.DemoServer:
                await startDemoServer();
                break;
        }
    }
};