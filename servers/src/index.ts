
import { getServerRedisClient, startWatchServerConfig } from "./serverConfigMgr";
import { startServers } from "./server";
import { PlayerAuthHelper } from "./shared/tsgfServer/auth/PlayerAuthHelper";
import { RoomHelper } from "./shared/tsgfServer/room/RoomHelper";


async function main() {
    startWatchServerConfig();

    PlayerAuthHelper.init(getServerRedisClient);
    RoomHelper.init(getServerRedisClient);

    await startServers();
};
main();


