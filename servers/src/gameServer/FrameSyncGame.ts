
import { MsgNotifySyncFrame } from "../shared/gameClient/protocols/MsgNotifySyncFrame";
import { MsgPlayerInpFrame } from "../shared/gameClient/protocols/MsgPlayerInpFrame";
import { MsgPlayerSendSyncState } from "../shared/gameClient/protocols/MsgPlayerSendSyncState";
import { ConnectionStatus } from "tsrpc";
import { FrameSyncExecutor } from "./FrameSyncExecutor";
import { ClientConnection, GameMsgCall, GameServer, GameWsServer } from "./GameServer";
import { GameConnMgr } from "./GameConnMgr";
import { IRoomInfo } from "../shared/tsgf/room/IRoomInfo";
import { IPlayer } from "../shared/tsgfServer/auth/Models";
import { EPlayerInputFrameType, IAfterFrames, IGameSyncFrame, IFramePlayerInput, IPlayerInputOperate } from "../shared/tsgf/room/IGameFrame";


/**帧同步游戏*/
export class FrameSyncGame {

    //private eventEmitter: EventEmitter = new EventEmitter();

    private _inGaming = false;
    /**是否已经开始了游戏*/
    get inGaming() {
        return this._inGaming;
    }

    /**帧同步执行器*/
    private frameSyncExecutor: FrameSyncExecutor;

    /** [开启useFrameSync] 是否启用随机要求客户端同步状态给服务端的功能,方便大大缩短追帧时间, 随机选一个连着的客户端要求发送*/
    public useRandomRequireConnSync = false;
    private _inRandomRequireConnSyncState: boolean = false;
    /**当前是否在随机要求连接同步游戏状态数据到服务端*/
    get inRandomRequireConnSyncState() {
        return this._inRandomRequireConnSyncState;
    }
    /**随机要求连接同步游戏状态数据到服务端的 定时器句柄*/
    private randomRequireConnSyncStateHD!: NodeJS.Timeout;
    /**随机要求连接同步游戏状态数据到服务端的 定时间隔*/
    public randomRequireConnSyncStateInvMs = 20000;
    private randomRequireConnSyncStateListenHD: Function | undefined;
    /**当前要求同步状态的玩家ID,即不是所有客户端发来的同步状态都使用的*/
    private requireSyncStatePlayerId: string | undefined;

    private roomInfo: IRoomInfo;
    private gameWsServer: GameWsServer;
    private gameConnMgr: GameConnMgr;
    /**
     * 构造函数
     * @param [syncFrameRate] 同步帧率(每秒多少帧),默认每秒60帧
     */
    constructor(roomInfo: IRoomInfo, gameWsServer: GameWsServer, gameConnMgr: GameConnMgr, syncFrameRate = 60) {
        this.roomInfo = roomInfo;
        this.gameWsServer = gameWsServer;
        this.gameConnMgr = gameConnMgr;
        this.frameSyncExecutor = new FrameSyncExecutor((msg) => this.onSyncOneFrame(msg), syncFrameRate);
    }

    /**销毁游戏数据,方便快速回收*/
    public dispose(): void {
        this.stopGame();
    }

    private onSyncOneFrame(msg: MsgNotifySyncFrame) {
        //TODO: 每帧去获取一下房间中所有玩家的连接,需要优化
        let playerConnList = this.gameConnMgr.getPlayersConnFromPlayerInfos(this.roomInfo.playerList);
        if (playerConnList.length <= 0) return;
        //广播给游戏中所有连接
        this.gameWsServer.broadcastMsg("NotifySyncFrame", msg, playerConnList);
    }


    /**
     * [同步中才有效]玩家输入操作帧
     * @date 2022/5/20 - 16:17:38
     *
     * @public
     * @param {IPlayer} player
     * @param {EPlayerInputFrameType} inpFrameType
     * @param setOthersProp 自行设置额外字段, 如帧输入类型是操作,则需要设置operates字段
     */
    public playerInpFrame(player: IPlayer, inpFrameType: EPlayerInputFrameType,
        setOthersProp?: (inpFrame: IFramePlayerInput) => void) {
        this.frameSyncExecutor.addPlayerInpFrame(player.playerInfo.playerId, inpFrameType, setOthersProp);
    }

    /**
     * 获取追帧数据(最后状态数据+追帧包)
     */
    public buildAfterFrames(): IAfterFrames {
        return this.frameSyncExecutor.buildAfterFrames();
    }
    /**
     * 请求帧数组
     * @date 2022/5/16 - 17:21:05
     *
     * @public
     * @param {number} beginFrameIndex 起始帧索引(包含)
     * @param {number} endFrameIndex 截止帧索引(包含)
     * @returns {IGameSyncFrame[]}
     */
    public requestFrames(beginFrameIndex: number, endFrameIndex: number): IGameSyncFrame[] {
        return this.frameSyncExecutor.requestFrames(beginFrameIndex, endFrameIndex);
    }

    /**
     * 同步游戏状态数据
     * @param stateData 
     * @param stateFrameIndex 
     */
    public syncStateData(stateData: any, stateFrameIndex: number): void {
        this.frameSyncExecutor.syncStateData(stateData, stateFrameIndex);
    }


    /**
     * 停止随机要求连接同步游戏状态数据给服务端
     */
    public stopRandomRequireConnSyncState(): void {
        this._inRandomRequireConnSyncState = false;
        clearInterval(this.randomRequireConnSyncStateHD);
    }
    /**
     * 开始随机要求连接同步游戏状态数据给服务端
     */
    public startRandomRequireConnSyncState(): void {
        this.stopRandomRequireConnSyncState();
        this._inRandomRequireConnSyncState = true;
        this.randomRequireConnSyncStateHD = setInterval(this.onRandomRequireConnSyncState.bind(this), this.randomRequireConnSyncStateInvMs);
    }

    /**玩家发送*/
    public playerSendSyncState(player: IPlayer, msg: MsgPlayerSendSyncState) {
        if (!this._inRandomRequireConnSyncState) return;
        //必须是服务端当前指定的(信任的),否则不使用这同步数据
        if (this.requireSyncStatePlayerId !== player?.playerInfo?.playerId) return;
        //同步服务端状态数据
        this.frameSyncExecutor.syncStateData(msg.stateData, msg.stateFrameIndex);
    }
    /**
     * 处理随机要求连接同步游戏状态数据给服务端
     */
    onRandomRequireConnSyncState(): void {
        //如果当前没有连接,直接返回
        if (this.roomInfo.playerList.length <= 0) return;

        let conn: ClientConnection | undefined;
        if (this.requireSyncStatePlayerId) {
            //已经指定过连接了,直接获取
            conn = this.gameConnMgr.getPlayerConn(this.requireSyncStatePlayerId);
        }
        if (!conn || conn.status != ConnectionStatus.Opened) {
            //没指定过或者之前指定的不能用了,则重新随机一个
            let connIndex = Math.floor(Math.random() * this.roomInfo.playerList.length);
            this.requireSyncStatePlayerId = this.roomInfo.playerList[connIndex].playerId;
            conn = this.gameConnMgr.getPlayerConn(this.requireSyncStatePlayerId);
        }

        conn!.sendMsg("RequirePlayerSyncState", {});
    }


    /**开始游戏,根据启用的功能,直接开始游戏服务支持*/
    public startGame(): void {
        this.frameSyncExecutor.startSyncFrame();
        if (this.useRandomRequireConnSync) {
            this.startRandomRequireConnSyncState();
        }
        this._inGaming = true;

        for (let playerInfo of this.roomInfo.playerList) {
            this.frameSyncExecutor.addPlayerInpFrame(playerInfo.playerId, EPlayerInputFrameType.PlayerEnterGame, 
                inpFrame => inpFrame.playerInfo = playerInfo);
        }
    }
    /**停止游戏内相关功能,并回收或重置相关数据*/
    public stopGame(): void {
        this._inGaming = false;
        this.frameSyncExecutor.stopSyncFrame();
        this.stopRandomRequireConnSyncState();
    }

}