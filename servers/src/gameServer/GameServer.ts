

import { ApiCall, ApiCallWs, MsgCall, MsgCallWs, WsConnection, WsServer, WsServerOptions } from "tsrpc";
import { GameConnMgr } from "./GameConnMgr";
import * as path from "path";
import { v4 } from "uuid";
import { serviceProto as GameServiceProto, ServiceType as GameServiceType } from "../shared/gameClient/protocols/serviceProto";
import { IGameServerInfo } from "../shared/hallClient/Models";
import { ClusterNodeClient } from "../shared/tsgfServer/cluster/ClusterNodeClient";
import { RedisClient } from "../shared/tsgfServer/redisHelper";
import { IGameServerCfg } from "../serverConfigMgr";
import { logger } from "../shared/tsgf/logger";
import { IRoomInfo } from "../shared/tsgf/room/IRoomInfo";
import { IPlayer } from "../shared/tsgfServer/auth/Models";
import { IPlayerInfo } from "../shared/tsgf/player/IPlayerInfo";
import { IResult } from "../shared/tsgf/Result";
import { GameServerAppRoomMgr } from "./GameServerAppRoomMgr";
import { MatchRequestServer } from "../shared/matchRequest/MatchRequestServer";

/**
 * 游戏服务器API专用的ApiCall类型，可用于获取Game服务对象
 * @date 2022/4/26 - 16:21:57
 *
 * @export
 * @typedef {GameApiCall}
 * @template req
 * @template res
 */
export type GameApiCall<req, res> = ApiCallWs<req, res, GameServiceType> & {
    getGameServer: () => GameServer;
};
/**
 * 游戏服务器Msg专用的MsgCall类型，可用于获取Game服务对象
 * @date 2022/4/26 - 16:21:57
 *
 * @export
 * @typedef {GameMsgCall}
 * @template msg
 */
export type GameMsgCall<msg> = MsgCallWs<msg, GameServiceType> & {
    getGameServer: () => GameServer;
};

/**游戏服务端的客户端连接*/
export type ClientConnection = WsConnection<GameServiceType>;

/**游戏的websocket服务类型*/
export type GameWsServer = WsServer<GameServiceType>;

export class GameServer {

    public server: WsServer<GameServiceType>;

    private getRedisClient: () => Promise<RedisClient>;
    private getGameServerCfg: () => IGameServerCfg;

    public gameConnMgr: GameConnMgr;
    public roomMgr: GameServerAppRoomMgr;
    public matchReqServer: MatchRequestServer;


    public gameClusterClient: ClusterNodeClient<IGameServerInfo>;

    constructor(getRedisClient: () => Promise<RedisClient>, getGameServerCfg: () => IGameServerCfg) {
        this.getRedisClient = getRedisClient;
        this.getGameServerCfg = getGameServerCfg;

        //固定值的使用 tmpGameServerCfg, 每次都获取配置的则用 this.getGameServerCfg()
        let tmpGameServerCfg = this.getGameServerCfg();
        this.server = new WsServer(GameServiceProto, {
            port: getGameServerCfg().listenPort,
            json: false,
            logger: logger,
        });
        //让Call能获取到本服务实例
        this.server.flows.preApiCallFlow.push((v: GameApiCall<any, any>) => {
            v.getGameServer = () => this;
            return v;
        });
        this.server.flows.preMsgCallFlow.push((v: GameMsgCall<any>) => {
            v.getGameServer = () => this;
            return v;
        });
        //设置游戏服务连接ID
        this.server.flows.postConnectFlow.push(async v => {
            v.connectionId = v4();
            return v;
        });

        //游戏服务器各管理模块启动
        this.matchReqServer = new MatchRequestServer(this.getRedisClient, false);
        this.gameConnMgr = new GameConnMgr(this.server);
        this.roomMgr = new GameServerAppRoomMgr(this.server, this.gameConnMgr, this.matchReqServer, this.getGameServerCfg().clusterNodeId);

        //集群
        this.gameClusterClient = new ClusterNodeClient<IGameServerInfo>(
            tmpGameServerCfg.clusterWSUrl, tmpGameServerCfg.clusterNodeId, tmpGameServerCfg.clusterKey,
            () => {
                return {
                    serverNodeId: tmpGameServerCfg.clusterNodeId,
                    serverName: this.getGameServerCfg().serverName,
                    serverUrl: this.getGameServerCfg().serverUrl,
                    clientCount: this.server!.connections.length,
                    extendData: this.getGameServerCfg().extendData,
                };
            }
        );

    }

    public async start(): Promise<void> {
        await this.server.autoImplementApi(path.resolve(__dirname, 'api'));
        await this.server.start();
        this.server.logger?.log("GameServer: 服务启动成功!");

        let joinErr = await this.gameClusterClient.joinCluster();
        if (joinErr) {
            this.server.logger?.log("GameServer: 加入集群服务器失败:" + joinErr);
        } else {
            this.server.logger?.log("GameServer: 加入集群服务器成功!");
        }
    }



}