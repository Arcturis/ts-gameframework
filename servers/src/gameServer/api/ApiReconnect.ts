
import { apiErrorThenClose } from "../../shared/tsgfServer/ApiBase";
import { ReqReconnect, ResReconnect } from "../../shared/gameClient/protocols/PtlReconnect";
import { GameApiCall } from "../GameServer";

export async function ApiReconnect(call: GameApiCall<ReqReconnect, ResReconnect>) {
    let gameServer = call.getGameServer();
    let ret = await gameServer.gameConnMgr.connReconnect(call.conn, call.req.playerToken);
    if (!ret.succ) {
        //5001表示拒绝继续重连尝试了,要求重新登录!
        return apiErrorThenClose(call, ret.err, { code: 5001 });
    }

    let roomInfo = await gameServer.roomMgr.getRoomInfo(call.conn.currPlayer);
    call.succ({
        currRoomInfo: roomInfo
    });
}