
import { ReqStartFrameSync, ResStartFrameSync } from "../../shared/gameClient/protocols/PtlStartFrameSync";
import { GameApiCall } from "../GameServer";

export async function ApiStartFrameSync(call: GameApiCall<ReqStartFrameSync, ResStartFrameSync>) {
    let gameServer = call.getGameServer();
    let gameRoom = await gameServer.roomMgr.getGameRoom(call.conn.currPlayer);
    if (!gameRoom) return call.error('玩家不在房间中！');
    gameRoom.startGameFrameSync(call.conn.currPlayer);
    return call.succ({});
}