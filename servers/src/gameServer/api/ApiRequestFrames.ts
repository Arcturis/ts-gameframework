
import { ReqRequestFrames, ResRequestFrames } from "../../shared/gameClient/protocols/PtlRequestFrames";
import { GameApiCall } from "../GameServer";

export async function ApiRequestFrames(call: GameApiCall<ReqRequestFrames, ResRequestFrames>) {
    if (call.req.beginFrameIndex < 0) {
        return call.error('beginFrameIndex需要大于等于0 ！', { code: 2001 });
    }
    if (call.req.endFrameIndex < call.req.beginFrameIndex) {
        return call.error('endFrameIndex需要大于等于beginFrameIndex！', { code: 2001 });
    }
    let gameServer = call.getGameServer();
    let gameRoom = await gameServer.roomMgr.getGameRoom(call.conn.currPlayer);
    if (!gameRoom) {
        return call.error('玩家不在房间中！', { code: 2001 });
    }
    if(!gameRoom.game.inGaming){
        return call.error('当前不在游戏中！', { code: 2002 });
    }
    let frames = gameRoom.game.requestFrames(call.req.beginFrameIndex, call.req.endFrameIndex);
    return call.succ({
        frames: frames,
    });
}