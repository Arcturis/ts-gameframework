
import { ReqRequestAfterFrames, ResRequestAfterFrames } from "../../shared/gameClient/protocols/PtlRequestAfterFrames";
import { GameApiCall } from "../GameServer";

export async function ApiRequestAfterFrames(call: GameApiCall<ReqRequestAfterFrames, ResRequestAfterFrames>) {
    
    let gameServer = call.getGameServer();
    let gameRoom = await gameServer.roomMgr.getGameRoom(call.conn.currPlayer);
    if (!gameRoom) {
        return call.error('玩家不在房间中！', { code: 2001 });
    }
    if(!gameRoom.game.inGaming){
        return call.error('当前不在游戏中！', { code: 2002 });
    }
    let afterFrames = gameRoom.game.buildAfterFrames();
    return call.succ(afterFrames);
}