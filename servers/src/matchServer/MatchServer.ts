import { BaseMatchServer } from "../shared/matchServer/BaseMatchServer";
import { logger } from "../shared/tsgf/logger";
import { RedisClient } from "../shared/tsgfServer/redisHelper";

export class MatchServer extends BaseMatchServer {

    constructor(clusterServerUrl: string, serverNodeId: string, clusterKey: string, getRedisClient: () => Promise<RedisClient>) {
        super(clusterServerUrl, serverNodeId, clusterKey, getRedisClient);
    }

    public async start(): Promise<void> {
        let joinErr = await this.joinCluster();
        if (joinErr) {
            logger?.error("MatchServer: 加入集群服务器失败:" + joinErr);
            return;
        } else {
            logger?.log("MatchServer: 加入集群服务器成功!");
        }
        await super.start();
    }
}