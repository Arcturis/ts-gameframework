import { CodeTemplate, TsrpcConfig } from 'tsrpc-cli';

const tsrpcConf: TsrpcConfig = {
    // Generate ServiceProto
    proto: [
        {
            //大厅服务器
            ptlDir: 'src/shared/hallClient/protocols', // Protocol dir
            output: 'src/shared/hallClient/protocols/serviceProto.ts', // Path for generated ServiceProto
            apiDir: 'src/hallServer/api',   // API dir
            docDir: 'docs/hallServer',     // API documents dir
            ptlTemplate: CodeTemplate.getExtendedPtl(),
            // msgTemplate: CodeTemplate.getExtendedMsg(),
        },
        {
            //游戏服务器
            ptlDir: 'src/shared/gameClient/protocols', // Protocol dir
            output: 'src/shared/gameClient/protocols/serviceProto.ts', // Path for generated ServiceProto
            apiDir: 'src/gameServer/api',   // API dir
            docDir: 'docs/gameServer',     // API documents dir
            ptlTemplate: CodeTemplate.getExtendedPtl(),
            // msgTemplate: CodeTemplate.getExtendedMsg(),
        },
        {
            //demo服务器
            ptlDir: 'src/shared/demoClient/protocols', // Protocol dir
            output: 'src/shared/demoClient/protocols/serviceProto.ts', // Path for generated ServiceProto
            apiDir: 'src/demoServer/api',   // API dir
            docDir: 'docs/demoServer',     // API documents dir
            ptlTemplate: CodeTemplate.getExtendedPtl(),
            // msgTemplate: CodeTemplate.getExtendedMsg(),
        },
        {
            //公共模块-集群管理器
            ptlDir: 'src/shared/tsgfServer/cluster/protocols',
            output: 'src/shared/tsgfServer/cluster/protocols/serviceProto.ts',
        }
    ],
    // Sync shared code
    sync: [
        {
            from: 'src/shared/tsgf',
            to: '../client/assets/shared/tsgf',
            type: 'copy'     // Change this to 'copy' if your environment not support symlink
        },
        {
            from: 'src/shared/hallClient',
            to: '../client/assets/shared/hallClient',
            type: 'copy'     // Change this to 'copy' if your environment not support symlink
        },
        {
            from: 'src/shared/gameClient',
            to: '../client/assets/shared/gameClient',
            type: 'copy'     // Change this to 'copy' if your environment not support symlink
        },
        {
            from: 'src/shared/demoClient',
            to: '../client/assets/shared/demoClient',
            type: 'copy'     // Change this to 'copy' if your environment not support symlink
        },
    ],
    // Dev server
    dev: {
        autoProto: true,        // Auto regenerate proto
        autoSync: true,         // Auto sync when file changed
        autoApi: true,          // Auto create API when ServiceProto updated
        watch: 'src',           // Restart dev server when these files changed
        entry: 'src/index.ts',  // Dev server command: node -r ts-node/register {entry}
    },
    // Build config
    build: {
        autoProto: true,        // Auto generate proto before build
        autoSync: true,         // Auto sync before build
        autoApi: true,          // Auto generate API before build
        outDir: 'dist',         // Clean this dir before build
    }
}
export default tsrpcConf;