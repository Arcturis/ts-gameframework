
# TSRPC API 接口文档

## 通用说明

- 所有请求方法均为 `POST`
- 所有请求均需加入以下 Header :
    - `Content-Type: application/json`

## 目录

- [玩家认证](#/Authorize)
- [解散房间](#/DismissRoom)
- [加入房间](#/JoinRoom)
- [离开房间](#/LeaveRoom)
- [断线重连](#/Reconnect)
- [请求追帧](#/RequestAfterFrames)
- [请求具体的帧数据](#/RequestFrames)
- [发送房间消息](#/SendRoomMsg)
- [房间内开始帧同步](#/StartFrameSync)
- [房间内停止帧同步](#/StopFrameSync)

---

## 玩家认证 <a id="/Authorize"></a>

需要连接后立即发出请求,否则超时将被断开连接

**路径**
- POST `/Authorize`

**请求**
```ts
interface ReqAuthorize {
    /** 玩家令牌,登录大厅时获得的 */
    playerToken: string,
    customPlayerStatus?: number,
    customProfile?: string,
    isRobot?: boolean
}
```

**响应**
```ts
interface ResAuthorize {
    /** 玩家ID */
    playerInfo: {
        /** 玩家ID */
        playerId: string,
        /** 显示名 */
        showName: string,
        /** 当前所在队伍id */
        teamId?: string,
        /** 自定义玩家状态 */
        customPlayerStatus: number,
        /** 自定义玩家信息 */
        customProfile?: string,
        /** 网络状态 */
        networkState: 0 | 1,
        /** 是否机器人 */
        isRobot: boolean
    }
}
```

---

## 解散房间 <a id="/DismissRoom"></a>

**路径**
- POST `/DismissRoom`

**请求**
```ts
interface ReqDismissRoom {
    roomId: string
}
```

**响应**
```ts
interface ResDismissRoom {
    roomInfo: {
        /** 房间ID */
        roomId: string,
        /** 房间名称 */
        roomName: string,
        /** 房主玩家ID，创建后，只有房主玩家的客户端才可以调用相关的管理操作 */
        ownerPlayerId: string,
        /** 是否私有房间，即不参与匹配, 但可以通过房间ID加入 */
        isPrivate: boolean,
        /** 如果参与匹配,则使用的匹配器标识 */
        matcherKey?: string,
        /** 是否不允许加人 */
        isForbidJoin: boolean,
        /** 创建房间的方式 */
        createType: 0 | 1,
        /** 进入房间的最大玩家数量 */
        maxPlayers: number,
        /** 房间类型字符串 */
        roomType?: string,
        /** 自定义房间属性字符串 */
        customProperties?: string,
        /** 玩家列表 */
        playerList: {
            /** 玩家ID */
            playerId: string,
            /** 显示名 */
            showName: string,
            /** 当前所在队伍id */
            teamId?: string,
            /** 自定义玩家状态 */
            customPlayerStatus: number,
            /** 自定义玩家信息 */
            customProfile?: string,
            /** 网络状态 */
            networkState: 0 | 1,
            /** 是否机器人 */
            isRobot: boolean
        }[],
        /** 帧率 */
        frameRate: number,
        /** 帧同步状态 */
        frameSyncState: 0 | 1,
        /** 创建房间时间戳（单位毫秒， new Date(createTime) 可获得时间对象） */
        createTime: number,
        /** 开始游戏时间戳（单位毫秒， new Date(createTime) 可获得时间对象）,0表示未开始 */
        startGameTime: number
    }
}
```

---

## 加入房间 <a id="/JoinRoom"></a>

**路径**
- POST `/JoinRoom`

**请求**
```ts
interface ReqJoinRoom {
    roomId: string
}
```

**响应**
```ts
interface ResJoinRoom {
    roomInfo: {
        /** 房间ID */
        roomId: string,
        /** 房间名称 */
        roomName: string,
        /** 房主玩家ID，创建后，只有房主玩家的客户端才可以调用相关的管理操作 */
        ownerPlayerId: string,
        /** 是否私有房间，即不参与匹配, 但可以通过房间ID加入 */
        isPrivate: boolean,
        /** 如果参与匹配,则使用的匹配器标识 */
        matcherKey?: string,
        /** 是否不允许加人 */
        isForbidJoin: boolean,
        /** 创建房间的方式 */
        createType: 0 | 1,
        /** 进入房间的最大玩家数量 */
        maxPlayers: number,
        /** 房间类型字符串 */
        roomType?: string,
        /** 自定义房间属性字符串 */
        customProperties?: string,
        /** 玩家列表 */
        playerList: {
            /** 玩家ID */
            playerId: string,
            /** 显示名 */
            showName: string,
            /** 当前所在队伍id */
            teamId?: string,
            /** 自定义玩家状态 */
            customPlayerStatus: number,
            /** 自定义玩家信息 */
            customProfile?: string,
            /** 网络状态 */
            networkState: 0 | 1,
            /** 是否机器人 */
            isRobot: boolean
        }[],
        /** 帧率 */
        frameRate: number,
        /** 帧同步状态 */
        frameSyncState: 0 | 1,
        /** 创建房间时间戳（单位毫秒， new Date(createTime) 可获得时间对象） */
        createTime: number,
        /** 开始游戏时间戳（单位毫秒， new Date(createTime) 可获得时间对象）,0表示未开始 */
        startGameTime: number
    }
}
```

---

## 离开房间 <a id="/LeaveRoom"></a>

**路径**
- POST `/LeaveRoom`

**请求**
```ts
interface ReqLeaveRoom {

}
```

**响应**
```ts
interface ResLeaveRoom {

}
```

---

## 断线重连 <a id="/Reconnect"></a>

**路径**
- POST `/Reconnect`

**请求**
```ts
interface ReqReconnect {
    /** 之前连接上的连接ID */
    playerToken: string
}
```

**响应**
```ts
interface ResReconnect {
    /** 当前所在房间信息,如果没在房间中则为 null */
    currRoomInfo: {
        /** 房间ID */
        roomId: string,
        /** 房间名称 */
        roomName: string,
        /** 房主玩家ID，创建后，只有房主玩家的客户端才可以调用相关的管理操作 */
        ownerPlayerId: string,
        /** 是否私有房间，即不参与匹配, 但可以通过房间ID加入 */
        isPrivate: boolean,
        /** 如果参与匹配,则使用的匹配器标识 */
        matcherKey?: string,
        /** 是否不允许加人 */
        isForbidJoin: boolean,
        /** 创建房间的方式 */
        createType: 0 | 1,
        /** 进入房间的最大玩家数量 */
        maxPlayers: number,
        /** 房间类型字符串 */
        roomType?: string,
        /** 自定义房间属性字符串 */
        customProperties?: string,
        /** 玩家列表 */
        playerList: {
            /** 玩家ID */
            playerId: string,
            /** 显示名 */
            showName: string,
            /** 当前所在队伍id */
            teamId?: string,
            /** 自定义玩家状态 */
            customPlayerStatus: number,
            /** 自定义玩家信息 */
            customProfile?: string,
            /** 网络状态 */
            networkState: 0 | 1,
            /** 是否机器人 */
            isRobot: boolean
        }[],
        /** 帧率 */
        frameRate: number,
        /** 帧同步状态 */
        frameSyncState: 0 | 1,
        /** 创建房间时间戳（单位毫秒， new Date(createTime) 可获得时间对象） */
        createTime: number,
        /** 开始游戏时间戳（单位毫秒， new Date(createTime) 可获得时间对象）,0表示未开始 */
        startGameTime: number
    } | null
}
```

---

## 请求追帧 <a id="/RequestAfterFrames"></a>

**路径**
- POST `/RequestAfterFrames`

**请求**
```ts
interface ReqRequestAfterFrames {

}
```

**响应**
```ts
interface ResRequestAfterFrames {
    /** 状态同步的数据(如果没启用状态同步则可忽略) */
    stateData: any,
    /** 状态同步所在帧索引(如果没启用状态同步则可忽略), 即追帧的索引(afterFrames)从下一帧开始 */
    stateFrameIndex: number,
    /** 要追帧数组, 不包含空帧, 客户端使用时需要注意判断每帧的帧索引,以及结合maxSyncFrameIndex来遍历每一帧 */
    afterFrames: {
        /** 帧索引 */
        frameIndex: number,
        /** 连接输入帧列表(如果玩家提交输入的频率很高,里面有重复的玩家数据,即同帧时系统不会合并同玩家输入) null则为空帧 */
        playerInputs: {
            /** 来源的玩家ID */
            playerId: string,
            /** 输入帧类型 */
            inputFrameType: 1 | 2 | 3 | 4,
            /** 玩家在本帧的操作列表. inputFrameType == EPlayerInputFrameType.Operates 有数据 */
            operates?: { [key: string]: any }[],
            [key: string]: any
        }[] | null,
        [key: string]: any
    }[],
    /** 当前最大帧索引 */
    maxSyncFrameIndex: number,
    /** 服务端同步帧率(每秒多少帧) */
    serverSyncFrameRate: number
}
```

---

## 请求具体的帧数据 <a id="/RequestFrames"></a>

**路径**
- POST `/RequestFrames`

**请求**
```ts
interface ReqRequestFrames {
    /** 起始帧索引(包含) */
    beginFrameIndex: number,
    /** 结束帧索引(包含) */
    endFrameIndex: number
}
```

**响应**
```ts
interface ResRequestFrames {
    /** 帧数组 */
    frames: {
        /** 帧索引 */
        frameIndex: number,
        /** 连接输入帧列表(如果玩家提交输入的频率很高,里面有重复的玩家数据,即同帧时系统不会合并同玩家输入) null则为空帧 */
        playerInputs: {
            /** 来源的玩家ID */
            playerId: string,
            /** 输入帧类型 */
            inputFrameType: 1 | 2 | 3 | 4,
            /** 玩家在本帧的操作列表. inputFrameType == EPlayerInputFrameType.Operates 有数据 */
            operates?: { [key: string]: any }[],
            [key: string]: any
        }[] | null,
        [key: string]: any
    }[]
}
```

---

## 发送房间消息 <a id="/SendRoomMsg"></a>

**路径**
- POST `/SendRoomMsg`

**请求**
```ts
interface ReqSendRoomMsg {
    /** 房间消息 */
    roomMsg: {
        /** 消息的接收类型，决定能接收到的玩家范围 */
        recvType: 1 | 2,
        /** 自定义消息字符串 */
        msg: string
    } | {
        recvType: 3,
        /** 接收本条消息的玩家ID列表 */
        recvPlayerList: string[],
        /** 自定义消息字符串 */
        msg: string
    }
}
```

**响应**
```ts
interface ResSendRoomMsg {

}
```

---

## 房间内开始帧同步 <a id="/StartFrameSync"></a>

**路径**
- POST `/StartFrameSync`

**请求**
```ts
interface ReqStartFrameSync {

}
```

**响应**
```ts
interface ResStartFrameSync {

}
```

---

## 房间内停止帧同步 <a id="/StopFrameSync"></a>

**路径**
- POST `/StopFrameSync`

**请求**
```ts
interface ReqStopFrameSync {

}
```

**响应**
```ts
interface ResStopFrameSync {

}
```

