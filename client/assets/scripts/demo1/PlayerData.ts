import { v2, Vec2 } from "cc";

export class PlayerData {
    public playerId = "";
    public showName = "";

    public pos:Vec2=v2();
    /**当前是否在移动中*/
    public inMoving = false;
    /**当前是否在攻击中*/
    public inAttacking = false;
    /**从X轴转到目标方向所需的旋转弧度(即 v2(Vec2.UNIT_X).rotate(signRad) 为当前方向*/
    public moveSignRadFromX: number = 0;
    /**最后的朝向(x代表三维的x,y代表三维的z)*/
    public lastDir: Vec2 = v2();
    /**每秒移动的距离*/
    public speed = 350;
}
