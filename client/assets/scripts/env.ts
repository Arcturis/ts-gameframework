
import { GameClient } from '../shared/gameClient/GameClient';
import { HallClient } from '../shared/hallClient/HallClient';
import { DemoClient } from '../shared/demoClient/DemoClient';
/// #if Miniapp
/// #code import { HttpClient, WsClient } from 'tsrpc-miniapp';
/// #else
import { HttpClient, WsClient } from 'tsrpc-browser';
/// #endif

/*
实现一下跨平台的各种客户端
*/

globalThis.getDemoClient = (demoServerUrl: string): DemoClient => {
    return new DemoClient((proto, opt) => {
        return new HttpClient(proto, opt);
    }, demoServerUrl);
};
globalThis.getHallClient = (gateServerUrl: string): HallClient => {
    return new HallClient((proto, opt) => {
        return new HttpClient(proto, opt);
    }, gateServerUrl);
};
globalThis.getGameClient = (playerToken: string, serverUrl: string): GameClient => {
    return new GameClient(playerToken, serverUrl, (proto, opt) => {
        return new WsClient(proto, opt);
    });
};

