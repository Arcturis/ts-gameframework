

/**外部定义好游戏大厅的服务器地址*/
declare let hallServerUrl: string;
/**外部定义好示例应用的用户系统服务器地址*/
declare let demoServerUrl: string;

/**
 * 外部实现:保存最后使用的重连ID
 * @param {string|null} serverUrl null表示删除
 * @param {string|null} reconnectId null表示删除
 */
declare function saveLastReconnectData(serverUrl: string | null, reconnectId: string | null): void;

/**
 * 外部实现:获取最后使用的重连ID
 */
declare function getLastReconnectData(): { serverUrl: string, reconnectId: string } | null;