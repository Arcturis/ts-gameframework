
import { _decorator, Component, Node } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('LinkMainNodeComponent')
export class LinkMainNodeComponent extends Component {
    @property({ type: Node, tooltip: "关联的主要节点, 一般用于子节点,可以快速找到主节点" })
    public MainNode?: Node;
}