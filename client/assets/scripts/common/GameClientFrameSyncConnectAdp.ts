
import { GameClient } from "../../shared/gameClient/GameClient";
import { IFrameSyncConnect } from "./FrameSyncExecutor";
import { IAfterFrames, IGameSyncFrame, IPlayerInputOperate } from "../../shared/tsgf/room/IGameFrame";

export class GameClientFrameSyncConnectAdp implements IFrameSyncConnect {

    private gameClient: GameClient;

    onAfterFrames: (msg: IAfterFrames) => void = (msg) => { };
    onSyncFrame: (syncFrame: IGameSyncFrame) => void = (msg) => { };
    onRequireSyncState: () => void = () => { };

    sendSyncState(stateData: object, stateFrameIndex: number): void {
        this.gameClient.playerSendSyncState(stateData, stateFrameIndex);
    }

    constructor(gameClient: GameClient) {
        this.gameClient = gameClient;
        this.gameClient.onRecvFrame = (frame) => {
            this.onSyncFrame(frame);
        };
        this.gameClient.onRequirePlayerSyncState = () => {
            this.onRequireSyncState();
        };
    }

}