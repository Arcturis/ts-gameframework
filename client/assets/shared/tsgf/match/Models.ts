
/**匹配类型*/
export enum EMatchFromType {
    /**单个或多个玩家要匹配，进已存在的或新的房间*/
    Player = 'Player',
    /**已经创建好的房间，支持匹配来人*/
    RoomJoinUs = 'RoomJoinUs',
}

export interface IMatchFromPlayer {
    /**要匹配的玩家ID*/
    playerIds: string[];
}
export interface IMatchFromRoomJoinUs {
    /**要人的房间ID*/
    roomId: string;
}

export interface IMatchParamsBase {
    /**匹配器标识，内置的匹配器: MatcherKeys，也可以使用自定义的匹配器*/
    matcherKey: string;
    /**匹配器参数，对应匹配器的参数配置*/
    matcherParams: any;
    /**匹配超时秒数, 0或者undefined则不做超时限制*/
    matchTimeoutSec?: number;
    /**房间最大玩家数, 只有相同的才会匹配在一起,并以此创建房间或者加入到一个一样值的可匹配房间*/
    maxPlayers: number;
}
export interface IMatchParamsFromPlayer extends IMatchParamsBase {
    /**发起类型是玩家*/
    matchFromType: EMatchFromType.Player;
    /**匹配发起的玩家信息*/
    matchFromInfo: IMatchFromPlayer;
}
export interface IMatchParamsFromRoomJoinUs extends IMatchParamsBase {
    /**发起类型是房间招人*/
    matchFromType: EMatchFromType.RoomJoinUs;
    /**匹配发起的玩家信息*/
    matchFromInfo: IMatchFromRoomJoinUs;
}

/**匹配参数*/
export type IMatchParams = IMatchParamsFromPlayer | IMatchParamsFromRoomJoinUs;


/**内置匹配器标识定义*/
export const MatcherKeys = {
    /**基础混战, matcherParams 类型对应为: IBaseMeleeMatcherParams*/
    BaseMelee: 'BaseMelee',
    /**基础对战, matcherParams 类型对应为: IBaseVSMatcherParams*/
    BaseVS: 'BaseVS',
};
/**基础房间混战匹配器的匹配属性*/
export interface IBaseMeleeMatcherParams {
    /**[Player匹配类型]至少几个玩家匹配,才算匹配成功(创建房间), 如果要匹配满,则设置为maxPlayers*/
    minPlayers: number;
    /**[Player匹配类型]生成结果后(满足最小玩家数但未满足最大玩家数时),是否继续开启房间招人匹配,直到满员*/
    resultsContinueRoomJoinUsMatch: boolean;
}
/**基础对战匹配器的匹配属性*/
export interface IBaseVSMatcherParams {

}

/*
let a: IMatchParams = {
    matchFromType: EMatchFromType.Player,
    matchFromInfo: {
        playerIds:[],
    },
    matcherKey: "BaseMelee",
    maxPlayers: 10,
    matcherParams: {},
};
let b: IMatchParams = {
    matchFromType: EMatchFromType.RoomJoinUs,
    matchFromInfo: {
        roomId:'123',
    },
    matcherKey: "BaseMelee",
    maxPlayers: 10,
    matcherParams: {
        maxPlayer:1
    },
};
let c!: IMatchParams;
if (c.matchFromType == EMatchFromType.RoomJoinUs) {
    let room = c.matchFromInfo;
} else if (c.matchFromType == EMatchFromType.Player) {
    let player = c.matchFromInfo;
}
*/




/**匹配结果*/
export interface IMatchResult {
    roomId: string;
    gameServerUrl: string;
}