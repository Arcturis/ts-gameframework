import { EApiCryptoMode } from "../../tsgf/apiCrypto/Models";
import { IRoomRegInfo } from "../../tsgf/room/IRoomInfo";
import { BaseRequest, BaseResponse, BaseConf } from "./base";

/**
 * 获取房间注册信息
*/
export interface ReqGetRoomRegInfo extends BaseRequest {
    roomId: string;
}

export interface ResGetRoomRegInfo extends BaseResponse {
    regInfo: IRoomRegInfo;
}

export const conf: BaseConf = {
    cryptoMode : EApiCryptoMode.None,
};