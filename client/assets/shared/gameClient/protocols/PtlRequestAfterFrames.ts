import { IAfterFrames } from "../../tsgf/room/IGameFrame";

/**
 * 请求追帧
 * 
 * */
export interface ReqRequestAfterFrames {
}

export interface ResRequestAfterFrames extends IAfterFrames{
    
}