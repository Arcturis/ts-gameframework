import { IRoomInfo } from "../../tsgf/room/IRoomInfo";


/**
 * 加入房间
 * 
 * */
export interface ReqJoinRoom {
    roomId: string;
}

export interface ResJoinRoom {
    roomInfo: IRoomInfo;
}