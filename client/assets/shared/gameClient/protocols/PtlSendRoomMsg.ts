import { IRoomMsg } from "../../../shared/tsgf/room/IRoomMsg";

/**
 * 发送房间消息
 * 
 * */
export interface ReqSendRoomMsg {
    roomMsg: IRoomMsg;
}

export interface ResSendRoomMsg {

}