

/**
 * 连接输入帧类型
 * @date 2022/2/16 - 下午4:18:09
 *
 * @export
 * @enum {number}
 */
export enum InputType {
    NewPlayer = "NewPlayer",
    RemovePlayer = "RemovePlayer",
    MoveDirStart = "MoveDirStart",
    MoveDirEnd = "MoveDirEnd",
}