import { assert } from 'chai';
//import { describe, before, it } from 'mocha';
import { FrameSyncExecutor, IFrameSyncConnect } from "../../assets/scripts/common/FrameSyncExecutor";
import { EPlayerInputFrameType, IPlayerInputOperate } from '../../assets/shared/tsgf/room/IGameFrame';

let testConnect: IFrameSyncConnect = {
    onSyncFrame: () => { },
    onRequireSyncState: () => { },
    sendSyncState: function (msg): void {
        console.log('sendSyncState', msg);
    }
};


it('推入三帧(空,输入,空),执行3次核对每次各字段值', async function () {
    let lastStateData = {
        allPlayers: {} as { [connId: string]: string }
    };
    let fse = new FrameSyncExecutor(testConnect, 'inputType', {
        execInput_NewPlayer: (playerId: string, inputFrame: IPlayerInputOperate) => {
            lastStateData.allPlayers[playerId] = inputFrame.userName;
        },
        execInput_RemovePlayer: (playerId: string, inputFrame: IPlayerInputOperate) => {
            delete lastStateData.allPlayers[playerId];
        },
    }, (dt, frameIndex) => {
        console.log('execOneFrame:', frameIndex);
    }, () => {
        console.log('getStateData:', lastStateData);
        return lastStateData
    });
    //模拟启动自动执行帧
    fse.executeFrameStop = false;

    var v = fse.allFrames.length;
    assert.ok(v === 0, "应为0,实际为" + v);
    v = fse.stateFrameIndex;
    assert.ok(v === -1, "应为-1,实际为" + v);
    v = fse.executeFrameIndex;
    assert.ok(v === -1, "应为-1,实际为" + v);
    v = fse.maxFrameIndex;
    assert.ok(v === -1, "应为-1,实际为" + v);

    //模拟有三帧要追,当前还没执行
    testConnect.onSyncFrame({
        frameIndex: 0,
        playerInputs: null,
    });
    testConnect.onSyncFrame({
        frameIndex: 1,
        playerInputs: [{
            playerId: "1",
            inputFrameType: EPlayerInputFrameType.PlayerEnterGame,
            playerInfo: {}
        }],
    });
    testConnect.onSyncFrame({
        frameIndex: 2,
        playerInputs: null,
    });

    v = fse.allFrames.length;
    assert.ok(v === 1, "应为1,实际为" + v);
    v = fse.maxFrameIndex;
    assert.ok(v === 2, "应为2,实际为" + v);
    v = fse.stateFrameIndex;
    assert.ok(v === -1, "应为-1,实际为" + v);
    v = fse.executeFrameIndex;
    assert.ok(v === -1, "应为-1,实际为" + v);
    v = fse.executeFrameIndexArrIndex;
    assert.ok(v === -1, "应为-1,实际为" + v);

    //执行0帧(是空帧)
    var frameEnd = fse.executeOneFrame(0.012);
    assert.ok(frameEnd === false, "应为false,实际为" + frameEnd);
    v = fse.allFrames.length;
    assert.ok(v === 1, "应为1,实际为" + v);
    v = fse.maxFrameIndex;
    assert.ok(v === 2, "应为2,实际为" + v);
    v = fse.stateFrameIndex;
    assert.ok(v === -1, "应为-1,实际为" + v);
    v = fse.executeFrameIndex;
    assert.ok(v === 0, "应为0,实际为" + v);
    v = fse.executeFrameIndexArrIndex;
    assert.ok(v === -1, "应为-1,实际为" + v);

    //执行1帧(非空帧)
    var frameEnd = fse.executeOneFrame(0.012);
    assert.ok(frameEnd === false, "应为false,实际为" + frameEnd);
    v = fse.allFrames.length;
    assert.ok(v === 1, "应为1,实际为" + v);
    v = fse.maxFrameIndex;
    assert.ok(v === 2, "应为2,实际为" + v);
    v = fse.stateFrameIndex;
    assert.ok(v === -1, "应为-1,实际为" + v);
    v = fse.executeFrameIndex;
    assert.ok(v === 1, "应为1,实际为" + v);
    v = fse.executeFrameIndexArrIndex;
    assert.ok(v === 0, "应为0,实际为" + v);

    //执行2帧(是空帧)
    var frameEnd = fse.executeOneFrame(0.012);
    assert.ok(frameEnd === true, "应为true,实际为" + frameEnd);
    v = fse.allFrames.length;
    assert.ok(v === 1, "应为1,实际为" + v);
    v = fse.maxFrameIndex;
    assert.ok(v === 2, "应为2,实际为" + v);
    v = fse.stateFrameIndex;
    assert.ok(v === -1, "应为-1,实际为" + v);
    v = fse.executeFrameIndex;
    assert.ok(v === 2, "应为2,实际为" + v);
    v = fse.executeFrameIndexArrIndex;
    assert.ok(v === 0, "应为0,实际为" + v);

});